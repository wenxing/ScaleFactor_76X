import math
import datetime

import ROOT
ROOT.gROOT.SetBatch(ROOT.kTRUE)

class sample_object:
    def __init__(self, name, crossSection, nEvents, type, color):
        self.name = name
        self.crossSection = crossSection
        self.nEvents = nEvents
        self.effectiveLumi = self.nEvents/self.crossSection
        self.type = type # 'data', 'MCDY', 'MCOther'
        self.color = color
        
        self.h_2D_fit  = {}
        self.h_2D_fit_tmp  = {}
        self.h_2D_cut_tmp  = {}
        self.h_2D_cut  = {}
        self.tfile = None
    
        self.tfilename = 'ntuples/out/%s_slices.root'%(self.name)
        self.tfile = ROOT.TFile(self.tfilename,'READ')

samples = {}
#samples['data_golden2015D'     ] = sample_object('data_golden2015D'     , -1,-1, 'data', ROOT.kBlack)
samples['data_silver'          ] = sample_object('data_silver_DBE_fakeRateNoTrig_v1'          , 1, 2612.9, 'data', ROOT.kBlack)
#samples['data_silverNotGolden' ] = sample_object('data_silverNotGolden' , -1,-1, 'data', ROOT.kBlack)

#samples['ZToEE'] = sample_object('ZToEE',    1975.0,  2997600, 'MCDY'   , ROOT.kGreen  )
samples['ZToEE'] = sample_object('ZToEE_powheg_FR',    1975.0,   2857302, 'MCDY'   , ROOT.kGreen  )
#samples['ZToEE'] = sample_object('DYToEE_amc', 6025.2, 20373245, 'MCDY'   , ROOT.kGreen  )
#samples['ZToEE'] = sample_object('DYToEE_mad', 6025.2, 256998880, 'MCDY'   , ROOT.kGreen  )
samples['ttbar'] = sample_object('ttbar_FR',    831.76,  97784324, 'MCOther', ROOT.kYellow )
samples['ST'] =    sample_object('ST_FR',       35.6,    999400,   'MCOther', ROOT.kRed )
samples['ST_anti'] =sample_object('ST_anti_FR', 35.6,    1000000,  'MCOther', ROOT.kOrange )
samples['WJets'] = sample_object('WJets_FR',    61526.7, 46893463, 'MCOther', ROOT.kBlue   )
#samples['WJets'] = sample_object('WJets',    61526.7, 46893463, 'MCOther', ROOT.kBlue   )
samples['WW'   ] = sample_object('WW_FR'   ,     118.7,   988418, 'MCOther', ROOT.kMagenta)
samples['ZToTT'] = sample_object('ZToTT_FR',    6025.2, 20373245, 'MCOther', ROOT.kCyan   )##?
samples['WZ'   ] = sample_object('WZ_FR'   ,     47.13,   1000000, 'MCOther', ROOT.kViolet+1)
samples['ZZ'   ] = sample_object('ZZ_FR'   ,     16.523,  985600,  'MCOther', ROOT.kSpring)
samples['GamJet1'   ] = sample_object('GJ40_100_FR',20790,  4424830,  'MCOther', ROOT.kCyan-10)
samples['GamJet2'   ] = sample_object('GJ100_200_FR',9238,  5116711,  'MCOther', ROOT.kCyan-8)
samples['GamJet3'   ] = sample_object('GJ200_400_FR',2305,  10490427,  'MCOther', ROOT.kCyan-6)
samples['GamJet4'   ] = sample_object('GJ400_600_FR',274.4, 2356919,  'MCOther', ROOT.kCyan-4)
samples['GamJet5'   ] = sample_object('GJ600_Inf_FR',93.46, 2456253,  'MCOther', ROOT.kCyan-2)


Jsons = ["golden","silver","combined"]

altCutnames = []
altCutnames.append('nominal'        )
altCutnames.append('noDEtaIn'       )
altCutnames.append('gsfIsEcalDriven')
altCutnames.append('noIsolation'    )

methodNames    = ['fit','cut']
variableNames  = ['Et','eta','phi','nVtx','Pt']
regionNames    = ['Barrel','Transition','Endcap']
chargeNames    = ['ep','em','ea']
tagChargeNames = ['tp','tm','ta']
OSSSNames      = ['OS','SS','AS']
HEEPNames      = ['probes','pass','fail']
PUWNames       = ["PUW","NoPUW"]

QCDname="data_silver_DBE_FakeRate_pow_gJ"

f_QCD=ROOT.TFile("ntuples/out/%s_slices.root"%(QCDname),"RECREATE")

ROOT.TH1.SetDefaultSumw2()

for mname in methodNames:
    for vname in variableNames:
        for rname in regionNames:
            for cname in chargeNames:
                for aname in altCutnames:
                    for PUWname in PUWNames:
                        for tname in tagChargeNames:
                            for jname in Jsons:
                                suffix_QCD_SS_fail = 'h_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s'%(mname, QCDname, vname, rname, cname, tname, 'SS', aname, jname, 'fail', PUWname)
                                suffix_QCD_OS_fail = 'h_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s'%(mname, QCDname, vname, rname, cname, tname, 'OS', aname, jname, 'fail', PUWname)
                                suffix_QCD_AS_fail = 'h_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s'%(mname, QCDname, vname, rname, cname, tname, 'AS', aname, jname, 'fail', PUWname)
                                suffix_QCD_SS_pass = 'h_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s'%(mname, QCDname, vname, rname, cname, tname, 'SS', aname, jname, 'pass', PUWname)
                                suffix_QCD_OS_pass = 'h_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s'%(mname, QCDname, vname, rname, cname, tname, 'OS', aname, jname, 'pass', PUWname)
                                suffix_QCD_AS_pass = 'h_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s'%(mname, QCDname, vname, rname, cname, tname, 'AS', aname, jname, 'pass', PUWname)
                                suffix_QCD_SS_probes = 'h_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s'%(mname, QCDname, vname, rname, cname, tname, 'SS', aname, jname, 'probes', PUWname)
                                suffix_QCD_OS_probes = 'h_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s'%(mname, QCDname, vname, rname, cname, tname, 'OS', aname, jname, 'probes', PUWname)
                                suffix_QCD_AS_probes = 'h_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s'%(mname, QCDname, vname, rname, cname, tname, 'AS', aname, jname, 'probes', PUWname)
                                suffix_SS_fail = 'h_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s'%(mname,'WW_FR' , vname, rname, cname, tname, 'SS', aname, jname, 'fail', PUWname)
                                h_2D_QCD_tmp_SS_fail = samples['WW'].tfile.Get(suffix_SS_fail).Clone(suffix_QCD_SS_fail)
                                h_2D_QCD_tmp_SS_fail.Scale(0)  
                                for sname in samples:
                                    suffix = 'h_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s_%s'%(mname, samples[sname].name, vname, rname, cname, tname, 'SS', aname, jname, 'fail', PUWname)
                                    h_2D_tmp = samples[sname].tfile.Get(suffix)
                                    if samples[sname].type == 'data':
                                        h_2D_QCD_tmp_SS_fail.Add(h_2D_tmp)
                                    if samples[sname].type == 'MCDY' or samples[sname].type == 'MCOther':
                                        w=samples["data_silver"].effectiveLumi/samples[sname].effectiveLumi
                                        h_2D_QCD_tmp_SS_fail.Add(h_2D_tmp, -1*w)
                                h_2D_QCD_tmp_OS_fail=h_2D_QCD_tmp_SS_fail.Clone(suffix_QCD_OS_fail)
                                h_2D_QCD_tmp_AS_fail=h_2D_QCD_tmp_SS_fail.Clone(suffix_QCD_AS_fail)
                                h_2D_QCD_tmp_AS_fail.Scale(2) 
                                h_2D_QCD_tmp_SS_pass=h_2D_QCD_tmp_SS_fail.Clone(suffix_QCD_SS_pass)
                                h_2D_QCD_tmp_OS_pass=h_2D_QCD_tmp_OS_fail.Clone(suffix_QCD_OS_pass)
                                h_2D_QCD_tmp_AS_pass=h_2D_QCD_tmp_AS_fail.Clone(suffix_QCD_AS_pass)
                                h_2D_QCD_tmp_SS_pass.Scale(0)    
                                h_2D_QCD_tmp_OS_pass.Scale(0)    
                                h_2D_QCD_tmp_AS_pass.Scale(0)    
                                h_2D_QCD_tmp_SS_probes=h_2D_QCD_tmp_SS_fail.Clone(suffix_QCD_SS_probes)
                                h_2D_QCD_tmp_OS_probes=h_2D_QCD_tmp_OS_fail.Clone(suffix_QCD_OS_probes)
                                h_2D_QCD_tmp_AS_probes=h_2D_QCD_tmp_AS_fail.Clone(suffix_QCD_AS_probes)
                                f_QCD.cd()
                                h_2D_QCD_tmp_SS_fail.Write()
                                h_2D_QCD_tmp_OS_fail.Write()
                                h_2D_QCD_tmp_AS_fail.Write()
                                h_2D_QCD_tmp_SS_pass.Write()
                                h_2D_QCD_tmp_OS_pass.Write()
                                h_2D_QCD_tmp_AS_pass.Write()
                                h_2D_QCD_tmp_SS_probes.Write()
                                h_2D_QCD_tmp_OS_probes.Write()
                                h_2D_QCD_tmp_AS_probes.Write()
f_QCD.Close()                                            






















