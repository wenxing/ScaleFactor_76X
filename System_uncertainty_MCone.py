
import math
import datetime

import ROOT
from array import array
ROOT.gROOT.SetBatch(ROOT.kTRUE)

class file_object:
    def __init__(self, name, type):
        self.name = name
        self.type = type # base or biase
        
        self.tfilename = 'ntuples/out/%s.root'%(self.name)
        self.tfile = ROOT.TFile(self.tfilename,'READ')


Analyse_name='nominal'

Files = {}

Files[Analyse_name]=file_object('SFPlots_%s'%(Analyse_name),'base')
Files['WJets_ScaleUp']=file_object('SFPlots_WJetsScaleUp','biase')
Files['WJets_ScaleDown']=file_object('SFPlots_WJetsScaleDown','biase')
Files['QCD_ScaleUp']=file_object('SFPlots_QCDScaleUp','biase')
Files['QCD_ScaleDown']=file_object('SFPlots_QCDScaleDown','biase')
Files['ttbar_ScaleUp']=file_object('SFPlots_ttbarScaleUp','biase')
Files['ttbar_ScaleDown']=file_object('SFPlots_ttbarScaleDown','biase')
Files['silver_ScaleUp']=file_object('SFPlots_silverUp','biase')
Files['silver_ScaleDown']=file_object('SFPlots_silverDown','biase')

methodNames    = ['cut']
variableNames  = ['Et','eta','phi','nVtx','Pt']
regionNames    = ['Barrel','Transition','Endcap','Barrel+Endcap']
chargeNames    = ['ep','em','ea']
tagChargeNames = ['tp','tm','ta']
OSSSNames      = ['OS','SS','AS']
HEEPNames      = ['pass']
PUWNames       = ["PUW","NoPUW"]
ApplyNames     = ['inc','exc']

altCutnames = []
altCutnames.append('nominal'        )
altCutnames.append('noDEtaIn'       )
altCutnames.append('gsfIsEcalDriven')
altCutnames.append('noIsolation'    )
date = '20160425'
dir = 'sys_plots'

Xaxis_title={}
Xaxis_title['Et'] = 'E_{T} (GeV)'
Xaxis_title['eta'] = '#eta'
Xaxis_title['phi'] = '#phi'
Xaxis_title['nVtx'] = 'Nvtx'
Xaxis_title['Pt'] = 'P_{T} (GeV/c)'

Range_title = {}
Range_title['Barrel'] = '|#eta| < 1.4442'
Range_title['Endcap'] = '1.566 < |#eta| < 2.5'
Range_title['Barrel+Endcap'] = '|#eta| < 1.4442 or 1.566 < |#eta| < 2.5'
Range_title['Transition'] = '1.4442 < |#eta| < 1.566'

fConstant = ROOT.TF1('fConstant', '[0]')
fConstant.SetParameters(0,1.0)
fConstant.SetLineColor(ROOT.kBlue)
fConstant.SetLineStyle(ROOT.kDashed)
fConstant.SetLineWidth(2)

Leg_MC = {}
Leg_MC['inc']='DY+non-DY'
Leg_MC['exc']='DY'
Leg_data = {}
Leg_data['inc']='data'
Leg_data['exc']='data(non-DY subtracted)'
ROOT.TH1.SetDefaultSumw2()

for alname in altCutnames:
    for mname in methodNames:
        for vname in variableNames:
            for cname in chargeNames:
                for tname in tagChargeNames:
                    for oname in OSSSNames:
                        for hname in HEEPNames:
                            for pname in PUWNames:
                                for apname in ApplyNames:
                                    for rname in regionNames:
                                        suffix = 'h_%s_eff_%s_%s_%s_%s_%s_%s_%s_%s_%s'%(mname, vname, apname, rname, cname, tname, hname, oname, alname, pname)
                                        stuff_data='h_%s_eff_%s_data_%s_%s_%s_%s_%s_%s_%s_%s_final'%(mname, vname, apname, rname, cname, tname, hname, oname, alname, pname)
                                        stuff_MC='h_%s_eff_%s_MC_%s_%s_%s_%s_%s_%s_%s_%s_final'%(mname, vname, apname, rname, cname, tname, hname, oname, alname, pname)
                                        stuff_ratio='h_%s_eff_%s_data_%s_%s_%s_%s_%s_%s_%s_%s__ratio_final'%(mname, vname, apname, rname, cname, tname, hname, oname, alname, pname)
                                        for fname in Files:
                                            if Files[fname].type == 'base':
                                                if Files[fname].tfile.Get(stuff_data) and Files[fname].tfile.Get(stuff_MC) and Files[fname].tfile.Get(stuff_ratio):
                                                    data_x = array('f')
                                                    data_x_err_L = array('f')
                                                    data_x_err_R = array('f')
                                                    data_y = array('f')
                                                    data_y_err_T = array('f')
                                                    data_y_err_B = array('f')
                                                    data_y_err_T_stat = array('f')
                                                    data_y_err_B_stat = array('f')
                                                    MC_x = array('f')
                                                    MC_x_err_L = array('f')
                                                    MC_x_err_R = array('f')
                                                    MC_y = array('f')
                                                    MC_y_err_T = array('f')
                                                    MC_y_err_B = array('f')
                                                    MC_y_err_T_stat = array('f')
                                                    MC_y_err_B_stat = array('f')
                                                    ratio_x = array('f')
                                                    ratio_x_err_L = array('f')
                                                    ratio_x_err_R = array('f')
                                                    ratio_data_y = array('f')
                                                    ratio_data_y_err_T = array('f')
                                                    ratio_data_y_err_B = array('f')
                                                    ratio_data_y_err_T_stat = array('f')
                                                    ratio_data_y_err_B_stat = array('f')
                                                    ratio_MC_y = array('f')
                                                    ratio_MC_y_err_T = array('f')
                                                    ratio_MC_y_err_B = array('f')
                                                    ratio_MC_y_err_T_stat = array('f')
                                                    ratio_MC_y_err_B_stat = array('f')
                                                    for i in range(1, Files[fname].tfile.Get(stuff_data).GetNbinsX()+1):
                                                        x_L =Files[fname].tfile.Get(stuff_data).GetBinLowEdge(i)
                                                        x_R =x_L + Files[fname].tfile.Get(stuff_data).GetBinWidth(i)
                                                        data_x.append((x_L+x_R)/2) 
                                                        data_x_err_L.append((x_R-x_L)/2)   
                                                        data_x_err_R.append((x_R-x_L)/2)   
                                                        data_y.append(Files[fname].tfile.Get(stuff_data).GetBinContent(i))          
                                                        data_y_err_T_stat.append(Files[fname].tfile.Get(stuff_data).GetBinError(i))
                                                        data_y_err_B_stat.append(Files[fname].tfile.Get(stuff_data).GetBinError(i))
                                                        MC_x.append((x_L+x_R)/2) 
                                                        MC_x_err_L.append((x_R-x_L)/2)   
                                                        MC_x_err_R.append((x_R-x_L)/2)   
                                                        MC_y.append(Files[fname].tfile.Get(stuff_MC).GetBinContent(i))                                                        
                                                        MC_y_err_T_stat.append(Files[fname].tfile.Get(stuff_MC).GetBinError(i))
                                                        MC_y_err_B_stat.append(Files[fname].tfile.Get(stuff_MC).GetBinError(i))
                                                        ratio_x.append((x_L+x_R)/2) 
                                                        ratio_x_err_L.append((x_R-x_L)/2)   
                                                        ratio_x_err_R.append((x_R-x_L)/2)   
                                                        #ratio_y.append(Files[fname].tfile.Get(stuff_ratio).GetBinContent(i)) 
                                                        #ratio_y_err_T_base.append(Files[fname].tfile.Get(stuff_ratio).GetBinError(i))
                                                        #ratio_y_err_B_base.append(Files[fname].tfile.Get(stuff_ratio).GetBinError(i))
 
                                                        data_central =  float( Files[fname].tfile.Get(stuff_data).GetBinContent(i) )
                                                        MC_central =    float( Files[fname].tfile.Get(stuff_MC).GetBinContent(i)   )
                                                        #ratio_central = float( Files[fname].tfile.Get(stuff_ratio).GetBinContent(i))
                                                        
                                                        data_y_err_T_stat_bin =float( Files[fname].tfile.Get(stuff_data).GetBinError(i)  )
                                                        data_y_err_B_stat_bin =float( Files[fname].tfile.Get(stuff_data).GetBinError(i)  )
                                                        MC_y_err_T_stat_bin =  float( Files[fname].tfile.Get(stuff_MC).GetBinError(i)    )
                                                        MC_y_err_B_stat_bin =  float( Files[fname].tfile.Get(stuff_MC).GetBinError(i)    )
                                                        #ratio_y_err_T_base_bin=float( Files[fname].tfile.Get(stuff_ratio).GetBinError(i) )
                                                        #ratio_y_err_B_base_bin=float( Files[fname].tfile.Get(stuff_ratio).GetBinError(i) )
                                                        data_y_err_T_bin=0   
                                                        data_y_err_B_bin=0   
                                                        MC_y_err_T_bin=0   
                                                        MC_y_err_B_bin=0   
                                                        ratio_y_err_T_bin=0   
                                                        ratio_y_err_B_bin=0   
                                                        for bfile in Files:
                                                            if Files[bfile].type == 'biase':
                                                               data_central_b = float( Files[bfile].tfile.Get(stuff_data).GetBinContent(i) )
                                                               MC_central_b = float( Files[bfile].tfile.Get(stuff_MC).GetBinContent(i) )
                                                        #       ratio_central_b = float( Files[bfile].tfile.Get(stuff_ratio).GetBinContent(i) )
                                                          #     print 'file %s: var %s : bin %d, data base %f, data biase %f'%(bfile, vname, i,data_central,data_central_b)
                                                               if data_central_b > data_central:
                                                                   data_y_err_T_bin = data_y_err_T_bin + math.pow(data_central_b-data_central,2)
                                                        #           print 'file %s: var %s : bin %d : more %f'%(bfile, vname, i, data_y_err_T_bin) 
                                                               else:
                                                                   data_y_err_B_bin = data_y_err_B_bin + math.pow(data_central_b-data_central,2)
                                                        #           print 'file %s: var %s : bin %d : less %f'%(bfile, vname, i, data_y_err_B_bin) 
                                                               if MC_central_b > MC_central:
                                                                   MC_y_err_T_bin = MC_y_err_T_bin + math.pow(MC_central_b-MC_central,2) 
                                                               else:
                                                                   MC_y_err_B_bin = MC_y_err_B_bin + math.pow(MC_central_b-MC_central,2)
                                                       #        if ratio_central_b > ratio_central:
                                                       #            ratio_y_err_T_bin = ratio_y_err_T_bin + math.pow(ratio_central_b-ratio_central,2) 
                                                       #        else:
                                                       #            ratio_y_err_B_bin = ratio_y_err_B_bin + math.pow(ratio_central_b-ratio_central,2)
                                                        data_y_err_T_stat_sys=float( math.sqrt(data_y_err_T_bin + pow(data_y_err_T_stat_bin,2)) )
                                                        data_y_err_B_stat_sys=float( math.sqrt(data_y_err_B_bin + pow(data_y_err_B_stat_bin,2)) )
                                                        MC_y_err_T_stat_sys=float( math.sqrt(MC_y_err_T_bin + pow(MC_y_err_T_stat_bin,2)) )
                                                        MC_y_err_B_stat_sys=float( math.sqrt(MC_y_err_B_bin + pow(MC_y_err_B_stat_bin,2)) )
                                                        data_y_err_T.append(data_y_err_T_stat_sys) 
                                                        data_y_err_B.append(data_y_err_B_stat_sys) 
                                                        MC_y_err_T.append(MC_y_err_T_stat_sys) 
                                                        MC_y_err_B.append(MC_y_err_B_stat_sys) 
                                                      #  ratio_y_err_T.append(math.sqrt(ratio_y_err_T_bin + pow(ratio_y_err_T_base_bin,2))) 
                                                      #  ratio_y_err_B.append(math.sqrt(ratio_y_err_B_bin + pow(ratio_y_err_B_base_bin,2)))
                                                        if MC_central != 0:
                                                            ratio_data_y.append(data_central/MC_central)  
                                                            ratio_data_y_err_T_stat.append(data_y_err_T_stat_bin/MC_central) 
                                                            ratio_data_y_err_B_stat.append(data_y_err_B_stat_bin/MC_central) 
                                                            ratio_data_y_err_T.append(data_y_err_T_stat_sys/MC_central) 
                                                            ratio_data_y_err_B.append(data_y_err_B_stat_sys/MC_central) 
                                                            ratio_MC_y.append(1)  
                                                            ratio_MC_y_err_T_stat.append(MC_y_err_T_stat_bin/MC_central) 
                                                            ratio_MC_y_err_B_stat.append(MC_y_err_B_stat_bin/MC_central) 
                                                            ratio_MC_y_err_T.append(MC_y_err_T_stat_sys/MC_central) 
                                                            ratio_MC_y_err_B.append(MC_y_err_B_stat_sys/MC_central) 
                                                        else:
                                                            ratio_data_y.append(0)  
                                                            ratio_data_y_err_T_stat.append(0) 
                                                            ratio_data_y_err_B_stat.append(0) 
                                                            ratio_data_y_err_T.append(0) 
                                                            ratio_data_y_err_B.append(0) 
                                                            ratio_MC_y.append(1)  
                                                            ratio_MC_y_err_T_stat.append(0) 
                                                            ratio_MC_y_err_B_stat.append(0) 
                                                            ratio_MC_y_err_T.append(0) 
                                                            ratio_MC_y_err_B.append(0) 
                                                            
 
                                                    Graph_eff_data=ROOT.TGraphAsymmErrors(len(data_x),data_x,data_y,data_x_err_L,data_x_err_R,data_y_err_B,data_y_err_T)
                                                    Graph_eff_MC  =ROOT.TGraphAsymmErrors(len(MC_x),MC_x,MC_y,MC_x_err_L,MC_x_err_R,MC_y_err_B,MC_y_err_T)
                                                    Graph_eff_data_ratio   =ROOT.TGraphAsymmErrors(len(ratio_x),ratio_x,ratio_data_y,ratio_x_err_L,ratio_x_err_R,ratio_data_y_err_T,ratio_data_y_err_B)
                                                    Graph_eff_MC_ratio   =ROOT.TGraphAsymmErrors(len(ratio_x),ratio_x,ratio_MC_y,ratio_x_err_L,ratio_x_err_R,ratio_MC_y_err_T,ratio_MC_y_err_B)
                                                    
                                                    Graph_eff_data_base =ROOT.TGraphAsymmErrors(len(data_x),data_x,data_y,data_x_err_L,data_x_err_R,data_y_err_B_stat,data_y_err_T_stat)
                                                    Graph_eff_MC_base =ROOT.TGraphAsymmErrors(len(MC_x),MC_x,MC_y,MC_x_err_L,MC_x_err_R,MC_y_err_B_stat,MC_y_err_T_stat)
                                                    Graph_eff_data_ratio_stat =ROOT.TGraphAsymmErrors(len(ratio_x),ratio_x,ratio_data_y,ratio_x_err_L,ratio_x_err_R,ratio_data_y_err_T_stat,ratio_data_y_err_B_stat)
                                                    Graph_eff_MC_ratio_stat =ROOT.TGraphAsymmErrors(len(ratio_x),ratio_x,ratio_MC_y,ratio_x_err_L,ratio_x_err_R,ratio_MC_y_err_T_stat,ratio_MC_y_err_B_stat)
                                                    canvas = ROOT.TCanvas('canvas','',100,100,800,800)
                                                    canvas.cd()
                                                    size = 0.2
                                                    pad1 = ROOT.TPad('pad1', '', 0.0, size, 1.0, 1.0, 0)
                                                    pad2 = ROOT.TPad('pad2', '', 0.0, 0.0, 1.0, size, 0)
                                                    pad1.Draw()
                                                    pad2.Draw()
                                                    pad1.SetLogx(vname == 'Et')
                                                    pad2.SetLogx(vname == 'Et')
                                                    pad1.SetGridx()
                                                    pad1.SetGridy()
                                                    pad1.SetTicky()
                                                    pad2.SetGridy()
                                                    pad2.SetTicky()
                                                    pad1.SetBottomMargin(0)
                                                    pad2.SetTopMargin(0)
                                                    pad2.SetBottomMargin(0.25)
                                                    pad1.cd()
                                                    Graph_eff_MC.GetYaxis().SetRangeUser(0.9*min(MC_y),1.15*max(MC_y))
                                                    Graph_eff_MC.GetXaxis().SetRangeUser(min(MC_x)-MC_x_err_L[0],max(MC_x)+MC_x_err_L[-1])
                                                    #Graph_eff_ratio.GetYaxis().SetRangeUser(0.9*min(ratio_y),1.1*max(ratio_y))
                                                    Graph_eff_MC_ratio.GetYaxis().SetRangeUser(0.94,1.06)
                                                    Graph_eff_MC_ratio.GetXaxis().SetRangeUser(min(ratio_x)-ratio_x_err_L[0],max(ratio_x)+ratio_x_err_L[-1])
                                                    if vname == 'eta' and rname == 'Barrel+Endcap':
                                                        Graph_eff_MC.GetYaxis().SetRangeUser(0.7,1.1)
                                                        Graph_eff_MC_ratio.GetYaxis().SetRangeUser(0.92,1.08)
                                                    Graph_eff_MC.SetFillColor(2)
                                                    Graph_eff_MC.SetFillStyle(3001)
                                                    Graph_eff_MC.GetYaxis().SetTitle('Efficiency')
                                                    Graph_eff_MC.GetYaxis().SetTitleOffset(1.0)
                                                    Graph_eff_MC.GetYaxis().SetTitleSize(0.05)
                                                    #Graph_eff_MC_base.SetFillColor(ROOT.kBlack)
                                                    Graph_eff_MC_base.SetFillColor(7)
                                                    #Graph_eff_MC_base.SetFillStyle(3004)
                                                    Graph_eff_MC_base.SetFillStyle(3001)
                                                    Graph_eff_data.SetMarkerStyle(10)
                                                    Graph_eff_data.SetLineColor(ROOT.kRed)
                                                    Graph_eff_data.SetLineWidth(2)
                                                    Graph_eff_data_base.SetMarkerStyle(10)
                                                    Graph_eff_data_base.SetLineColor(ROOT.kBlue)
                                                    Graph_eff_data_base.SetLineWidth(2)
                                                    Graph_eff_MC_ratio.SetFillColor(2)
                                                    Graph_eff_MC_ratio.SetFillStyle(3001)
                                                    Graph_eff_MC_ratio_stat.SetFillColor(7)
                                                    Graph_eff_MC_ratio_stat.SetFillStyle(3001)
                                                    #Graph_eff_MC_ratio_stat.SetMarkerStyle(10)
                                                    Graph_eff_MC_ratio.GetXaxis().SetTitle(Xaxis_title[vname])
                                                    Graph_eff_MC_ratio.GetYaxis().SetTitle('data/MC')
                                                    Graph_eff_MC_ratio.GetYaxis().SetNdivisions(606)
                                                    Graph_eff_MC_ratio.GetXaxis().SetTitleSize(0.1)
                                                    Graph_eff_MC_ratio.GetYaxis().SetTitleSize(0.2)
                                                    Graph_eff_MC_ratio.GetXaxis().SetLabelSize(0.12)
                                                    Graph_eff_MC_ratio.GetYaxis().SetLabelSize(0.1)
                                                    Graph_eff_MC_ratio.GetYaxis().SetTitleOffset(0.2)
                                                    Graph_eff_MC_ratio.GetXaxis().SetMoreLogLabels()
                                                    Graph_eff_data_ratio.SetMarkerStyle(10)
                                                    Graph_eff_data_ratio.SetLineColor(ROOT.kRed)
                                                    Graph_eff_data_ratio.SetLineWidth(2)
                                                    Graph_eff_data_ratio_stat.SetMarkerStyle(10)
                                                    Graph_eff_data_ratio_stat.SetLineColor(ROOT.kBlue)
                                                    Graph_eff_data_ratio_stat.SetLineWidth(2)
                                                    
                                                    legend = ROOT.TLegend(0.15, 0.65, 0.8, 0.85)
                                                    legend.SetTextSize(0.03)
                                                    legend.SetFillStyle(0)
                                                    legend.SetShadowColor(0)
                                                    legend.SetBorderSize(0)
                                                    legend.AddEntry(Graph_eff_MC_base  ,Leg_MC[apname]+'(stat.)' , 'f')
                                                    legend.AddEntry(Graph_eff_MC       ,Leg_MC[apname]+'(stat.#oplus syst.)' , 'f')
                                                    if apname == 'inc':
                                                        legend.AddEntry(Graph_eff_data_base,Leg_data[apname] , 'epl')
                                                    if apname == 'exc':
                                                        legend.AddEntry(Graph_eff_data_base,Leg_data[apname]+'(stat.)' , 'epl')
                                                        legend.AddEntry(Graph_eff_data     ,Leg_data[apname]+'(stat.#oplus syst.)' , 'epl')

                                                    Graph_eff_MC.SetTitle("")
                                                    Graph_eff_MC.Draw('a2')
                                                    Graph_eff_MC_base.Draw('2same')
                                                    Graph_eff_data.Draw('P')
                                                    Graph_eff_data_base.Draw('P')
                                                    legend.Draw()
                                                    region_label = ROOT.TLatex(0.15, 0.6, Range_title[rname])
                                                    region_label.SetTextSize(0.03)
                                                    region_label.SetNDC()
                                                    Y_p=0.92
                                                    lumi_label = ROOT.TLatex(0.6, Y_p, '2.61 fb^{-1} (13 TeV)')
                                                    lumi_label.SetTextSize(0.05)
                                                    lumi_label.SetNDC()
                                                    CMS_label = ROOT.TLatex(0.1, Y_p, 'CMS')
                                                    CMS_label.SetTextSize(0.06)
                                                    CMS_label.SetTextFont(61)
                                                    CMS_label.SetNDC()
                                                    pre_label = ROOT.TLatex(0.2, Y_p, 'Preliminary')
                                                    pre_label.SetTextSize(0.06)
                                                    pre_label.SetTextFont(52)
                                                    pre_label.SetNDC()
                                                    lumi_label.Draw()
                                                    CMS_label.Draw()
                                                    pre_label.Draw()
                                                    region_label.Draw()
                                                    pad2.cd()
                                                    Graph_eff_MC_ratio.SetTitle("")
                                                    Graph_eff_MC_ratio_stat.SetTitle("")
                                                    Graph_eff_data_ratio.SetTitle("")
                                                    Graph_eff_data_ratio_stat.SetTitle("")
                                                    Graph_eff_MC_ratio.Draw('a2')
                                                    Graph_eff_MC_ratio_stat.Draw('2same')
                                                    Graph_eff_data_ratio.Draw('P')
                                                    Graph_eff_data_ratio_stat.Draw('P')
                                                    Graph_eff_data_ratio.Fit(fConstant)
                                                    Graph_eff_data_ratio.GetFunction('fConstant').SetLineColor(6)
                                                    a_value = fConstant.GetParameter(0)
                                                    a_error = fConstant.GetParError (0)
                                                    params_label = ROOT.TLatex(0.85, 0.85, 'SF = %.3f (#pm %.3f)'%(a_value, a_error))
                                                    params_label.SetNDC()
                                                    params_label.SetTextAlign(32)
                                                    params_label.SetTextSize(0.12)
                                                    params_label.Draw()
                                                    legend_ratio = ROOT.TLegend(0.63, 0.85, 0.67,0.9)
                                                    legend_ratio.SetFillStyle(0)
                                                    legend_ratio.SetShadowColor(0)
                                                    legend_ratio.SetBorderSize(0)
                                                    legend_ratio.AddEntry(Graph_eff_data_ratio_stat,'' , 'epl')
                                                    #legend_ratio.Draw()
                                                    line_1 = ROOT.TLine(min(ratio_x)-ratio_x_err_L[0], 1, max(ratio_x)+ratio_x_err_L[-1], 1)
                                                    line_1.SetLineColor(ROOT.kRed)
                                                    line_1.SetLineStyle(1)
                                                    #line_1.Draw()
                                                    canvas.Print('%s/%s/%s.png'%(dir,date,suffix)) 
                                                    
