import math
import datetime

import ROOT
ROOT.gROOT.SetBatch(ROOT.kTRUE)

from mod_settings import *
from mod_variable import variables

##########################################################################################
#                                     Create samples                                     #
##########################################################################################
class sample_object:
    def __init__(self, name, crossSection, nEvents, type, color):
        self.name = name
        self.crossSection = crossSection
        self.nEvents = nEvents
        self.effectiveLumi = self.nEvents/self.crossSection
        self.type = type # 'data', 'MCDY', 'MCOther'
        self.color = color
        
        self.h_2D_fit  = {}
        self.h_2D_fit_tmp  = {}
        self.h_2D_cut_tmp  = {}
        self.h_2D_cut  = {}
        self.tfile = None
        
        self.card = nominal_card
    
    def set_card(self, card):
        self.card = card
        self.tfilename = '%s%s_slices.root'%(self.card.out_ntuple_prefix,self.name)
        self.tfile = ROOT.TFile(self.tfilename,'READ')
    
    def load_histograms_from_file(self):
        for vname in self.card.variable_names:
            for rname in self.card.region_names:
                for cname in self.card.charge_names:
                    for HEEPname in self.card.HEEP_names:
                        for OSSSname in self.card.OSSS_names:
                            for aname in self.card.altCut_names:
                                for PUWname in self.card.PUW_names:
                                    for tname in self.card.tagCharge_names:
                                        suffix = '%s_%s_%s_%s_%s_%s_%s_%s_%s_%s'%(self.name, vname, rname, cname, tname, OSSSname, aname, self.card.json, HEEPname, PUWname)
                                        args = (vname,rname,cname,tname,HEEPname,OSSSname,aname,PUWname)
                                    
                                        hName_fit  = 'h_fit_%s' %suffix
                                        hName_cut  = 'h_cut_%s' %suffix
                                    
                                        self.h_2D_fit_tmp[args] = self.tfile.Get(hName_fit)
                                        if not self.h_2D_fit_tmp[args]:
                                            print 'error: not found'
                                            print hName_fit
                                            print self.tfilename 
                                        nbX_fit = self.h_2D_fit_tmp[args].GetNbinsX()
                                        nbY_fit = self.h_2D_fit_tmp[args].GetNbinsY()
                                        for binX in range(1,nbX_fit+1):
                                            self.h_2D_fit_tmp[args].SetBinContent(binX,    0,0)
                                            self.h_2D_fit_tmp[args].SetBinContent(binX,nbY_fit+1,0)
                                        for binY in range(1,nbY_fit+1):
                                            self.h_2D_fit_tmp[args].SetBinContent(    0,binY,0)
                                            self.h_2D_fit_tmp[args].SetBinContent(nbX_fit+1,binY,0)
                                     #   self.h_2D_fit[args] = self.tfile.Get(hName_fit)
                                        self.h_2D_fit[args] = self.h_2D_fit_tmp[args]
                                        
                                        self.h_2D_cut_tmp[args] = self.tfile.Get(hName_cut)
                                        nbX_cut = self.h_2D_cut_tmp[args].GetNbinsX()
                                        nbY_cut = self.h_2D_cut_tmp[args].GetNbinsY()
                                        for binX in range(1,nbX_cut+1):
                                            self.h_2D_cut_tmp[args].SetBinContent(binX,    0,0)
                                            self.h_2D_cut_tmp[args].SetBinContent(binX,nbY_cut+1,0)
                                        for binY in range(1,nbY_cut+1):
                                            self.h_2D_cut_tmp[args].SetBinContent(    0,binY,0)
                                            self.h_2D_cut_tmp[args].SetBinContent(nbX_cut+1,binY,0)

                                     #   self.h_2D_cut[args] = self.tfile.Get(hName_cut)
                                        self.h_2D_cut[args] = self.h_2D_cut_tmp[args] 
                                            
    def get_fit_histogram(self, args):
        h = self.h_2D_fit[args]
        return h

    def get_cut_histogram(self, args):
        h = self.h_2D_cut[args]
        return h
samples = {}
#samples['data_0T50ns'          ] = sample_object('data_0T50ns'          , -1,-1, 'data', ROOT.kBlack)
#samples['data_0T25ns'          ] = sample_object('data_0T25ns'          , -1,-1, 'data', ROOT.kBlack)
#samples['data_golden2015BC50ns'] = sample_object('data_golden2015BC50ns', -1,-1, 'data', ROOT.kBlack)
#samples['data_golden2015C25ns' ] = sample_object('data_golden2015C25ns' , -1,-1, 'data', ROOT.kBlack)
samples['data_golden2015D'     ] = sample_object('data_golden2015D_EndcapCorr_v1'     , -1,-1, 'data', ROOT.kBlack)
samples['data_silverUp'          ] = sample_object('data_silver_EndcapCorr_v1'          , -1,-1, 'data', ROOT.kBlack)
samples['data_silverDown'          ] = sample_object('data_silver_EndcapCorr_v1'          , -1,-1, 'data', ROOT.kBlack)
samples['data_silver'          ] = sample_object('data_silver_EndcapCorr_v1'          , -1,-1, 'data', ROOT.kBlack)
#samples['data_silver'          ] = sample_object('data_silver_EndcapCorr'          , -1,-1, 'data', ROOT.kBlack)
#samples['data_silver'          ] = sample_object('data_silver_m80'          , -1,-1, 'data', ROOT.kBlack)
samples['data_silverNotGolden' ] = sample_object('data_silverNotGolden_EndcapCorr_v1' , -1,-1, 'data', ROOT.kBlack)

samples['ZToEE'] = sample_object('ZToEE_powheg_pu',       1975.0,   2857302, 'MCDY'   , ROOT.kGreen  )
#samples['ZToEE'] = sample_object('DYToEE_amc_pu', 6025.2, 20373245, 'MCDY'   , ROOT.kGreen  )
#samples['ZToEE'] = sample_object('DYToEE_mad_pu', 6025.2, 256998880, 'MCDY'   , ROOT.kGreen  )
samples['ttbar'] = sample_object('ttbar_pu',    831.76,  97784324, 'MCOther', ROOT.kYellow )
samples['ST'] =    sample_object('ST_pu',       35.6,    999400,   'MCOther', ROOT.kRed )
samples['ST_anti'] =sample_object('ST_anti_pu', 35.6,    1000000,  'MCOther', ROOT.kOrange )
samples['WJets'] = sample_object('WJets_pu',   61526.7, 46893463, 'MCOther', ROOT.kBlue   )
#samples['WJets'] = sample_object('WJets',    0.00001, 46893463, 'MCOther', ROOT.kBlue   )
samples['WW'   ] = sample_object('WW_pu'   ,    118.7,   988418, 'MCOther', ROOT.kMagenta)
samples['ZToTT'] = sample_object('ZToTT_pu',    6025.2,  20373245, 'MCOther', ROOT.kCyan   )##?
samples['WZ'   ] = sample_object('WZ_pu'   ,    47.13,   1000000, 'MCOther', ROOT.kViolet+1)
samples['ZZ'   ] = sample_object('ZZ_pu'   ,    16.523,  985600,  'MCOther', ROOT.kSpring)
samples['GamJet1'   ] = sample_object('GamJet_40_100_pu',20790,  4424830,  'MCOther', ROOT.kCyan-10)
samples['GamJet2'   ] = sample_object('GamJet_100_200_pu',9238,  5116711,  'MCOther', ROOT.kCyan-10)
samples['GamJet3'   ] = sample_object('GamJet_200_400_pu',2305,  10490427,  'MCOther', ROOT.kCyan-10)
samples['GamJet4'   ] = sample_object('GamJet_400_600_pu',274.4, 2356919,  'MCOther', ROOT.kCyan-10)
samples['GamJet5'   ] = sample_object('GamJet_600_Inf_pu',93.46, 2456253,  'MCOther', ROOT.kCyan-10)
#samples['QCD'  ] = sample_object('QCD_pow_EndcapCorr' , 1,       2612.9,  'MCOther', ROOT.kYellow-5)
#samples['QCD'  ] = sample_object('QCD_amc_EndcapCorr' , 1,       2612.9,  'MCOther', ROOT.kYellow-5)
#samples['QCD'  ] = sample_object('QCD_mad_EndcapCorr' , 1,       2612.9,  'MCOther', ROOT.kYellow-5)
samples['QCD'  ] = sample_object('data_silver_DBE_fakeRateNoTrig_pu' , 1,        2612.9,  'MCOther', ROOT.kYellow-5)
#samples['QCD'  ] = sample_object('data_silverNotGolden_DBE_fakeRate_noTrig', 1,  366.69,  'MCOther', ROOT.kYellow-5)
#samples['QCD'  ] = sample_object('data_golden2015D_DBE_fakeRate_noTrig' , 1,     2246.26,  'MCOther', ROOT.kYellow-5)


#samples['ZToEE'] = sample_object('ZToEE_powheg_m80', 1975.0,   2857302, 'MCDY'   , ROOT.kGreen  )
#samples['ttbar'] = sample_object('ttbar_m80',    831.76,  97784324, 'MCOther', ROOT.kYellow )
#samples['ST'] =    sample_object('ST_m80',       35.6,    999400,   'MCOther', ROOT.kRed )
#samples['ST_anti'] =sample_object('ST_anti_m80', 35.6,    1000000,  'MCOther', ROOT.kOrange )
#samples['WJets'] = sample_object('WJets_m80',    61526.7, 46893463, 'MCOther', ROOT.kBlue   )
#samples['WW'   ] = sample_object('WW_m80'   ,     118.7,   988418, 'MCOther', ROOT.kMagenta)
#samples['ZToTT'] = sample_object('ZToTT_m80',    6025.2, 20373245, 'MCOther', ROOT.kCyan   )##?
#samples['WZ'   ] = sample_object('WZ_m80'   ,     47.13,   1000000, 'MCOther', ROOT.kViolet+1)
#samples['ZZ'   ] = sample_object('ZZ_m80'   ,     16.523,  985600,  'MCOther', ROOT.kSpring)

for sname in samples:
    styles[sname] = style_object(samples[sname].color, 20)
 
