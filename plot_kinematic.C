#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <THStack.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TRandom3.h>
#include <TLorentzVector.h>
#include <time.h>
#include <iostream>
#include <TPaveText.h>
#include <TText.h>
#include <TLine.h>
#include <TLegend.h>
#include <sstream>
#include "Fake_rate.C"
float data_silver=2612.9;
TString Date="20160422";

struct File_plots{


float lumi;
float scale_to_data;
TString file_n;
TString chain_n;
TString sample_n;
bool isData;
bool doPUW;
float Et_barrel[31]={35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,65,70,75,100,250};
float Et_endcap[13]={35,36,37,38,39,40,41,42,43,45,50,70,250};
float Et_transition[4]={35,41,50,250};
TH1F *H_Et_barrel_all; 
TH1F *H_Et_endcap_all; 
TH1F *H_Et_transition_all; 
TH1F *H_Et_barrel_pass; 
TH1F *H_Et_endcap_pass; 
TH1F *H_Et_transition_pass; 
TH1F *H_Et_barrel_fail; 
TH1F *H_Et_endcap_fail; 
TH1F *H_Et_transition_fail; 

TH1F *H_eta_barrel_all; 
TH1F *H_eta_endcap_all; 
TH1F *H_eta_transition_all; 
TH1F *H_eta_barrel_pass; 
TH1F *H_eta_endcap_pass; 
TH1F *H_eta_transition_pass; 
TH1F *H_eta_barrel_fail; 
TH1F *H_eta_endcap_fail; 
TH1F *H_eta_transition_fail; 
TH1F *H_phi_barrel_all; 
TH1F *H_phi_endcap_all; 
TH1F *H_phi_transition_all; 
TH1F *H_phi_barrel_pass; 
TH1F *H_phi_endcap_pass; 
TH1F *H_phi_transition_pass; 
TH1F *H_phi_barrel_fail; 
TH1F *H_phi_endcap_fail; 
TH1F *H_phi_transition_fail; 
TH1F *H_tag_Et;
TH1F *H_tag_Eta;
TH1F *H_tag_Phi;


TH1F *H_RePt_barrel_all; 
TH1F *H_RePt_endcap_all; 
TH1F *H_RePt_transition_all; 
TH1F *H_RePt_barrel_pass; 
TH1F *H_RePt_endcap_pass; 
TH1F *H_RePt_transition_pass; 
TH1F *H_RePt_barrel_fail; 
TH1F *H_RePt_endcap_fail; 
TH1F *H_RePt_transition_fail; 

TH1F *H_RePt_BE_all; 
TH1F *H_RePt_BE_pass; 
TH1F *H_RePt_BE_fail; 

TH1F *H_Mee_barrel_all; 
TH1F *H_Mee_endcap_all; 
TH1F *H_Mee_transition_all; 
TH1F *H_Mee_barrel_pass; 
TH1F *H_Mee_endcap_pass; 
TH1F *H_Mee_transition_pass; 
TH1F *H_Mee_barrel_fail; 
TH1F *H_Mee_endcap_fail; 
TH1F *H_Mee_transition_fail; 
Int_t N_out=0;
bool do_fake_rate;

File_plots(TString sample_name, TString file_name, TString chain_name, float Nevents, float crosssection, bool isData_tmp, bool PUW_tmp, bool Do_fake_rate ){

lumi=Nevents/crosssection;
scale_to_data = data_silver/lumi;
sample_n=sample_name;
file_n=file_name;
chain_n=chain_name;
isData=isData_tmp;
doPUW=PUW_tmp;
do_fake_rate = Do_fake_rate;

H_tag_Et = new TH1F(sample_n+"_Tag_Et","",35, 30,100);
H_tag_Eta = new TH1F(sample_n+"_Tag_Eta","",100, -5,5);
H_tag_Phi = new TH1F(sample_n+"_Tag_Phi","",100, -5,5);
int N_bin_B=35;
int N_bin_E=35;
int N_bin_T=7;
float Et_low=30;
float Et_hight=100;
H_Et_barrel_all = new TH1F(sample_n+"_Et_barrel_all","",N_bin_B, Et_low, Et_hight);
H_Et_endcap_all = new TH1F(sample_n+"_Et_endcap_all","",N_bin_E, Et_low, Et_hight);
H_Et_transition_all = new TH1F(sample_n+"_Et_transition_all","",N_bin_T, Et_low, Et_hight);
H_Et_barrel_pass = new TH1F(sample_n+"_Et_barrel_pass","",N_bin_B, Et_low, Et_hight);
H_Et_endcap_pass = new TH1F(sample_n+"_Et_endcap_pass","",N_bin_E, Et_low, Et_hight);
H_Et_transition_pass = new TH1F(sample_n+"_Et_transition_pass","",N_bin_T, Et_low, Et_hight);
H_Et_barrel_fail = new TH1F(sample_n+"_Et_barrel_fail","",N_bin_B, Et_low, Et_hight);
H_Et_endcap_fail = new TH1F(sample_n+"_Et_endcap_fail","",N_bin_E, Et_low, Et_hight);
H_Et_transition_fail = new TH1F(sample_n+"_Et_transition_fail","",N_bin_T, Et_low, Et_hight);

int N_eta_B=60;
int N_eta_E=60;
int N_eta_T=60;
float eta_low=-3;
float eta_hight=3;
H_eta_barrel_all = new TH1F(sample_n+"_eta_barrel_all","",N_eta_B, eta_low, eta_hight);
H_eta_endcap_all = new TH1F(sample_n+"_eta_endcap_all","",N_eta_E, eta_low, eta_hight);
H_eta_transition_all = new TH1F(sample_n+"_eta_transition_all","",N_eta_T, eta_low, eta_hight);
H_eta_barrel_pass = new TH1F(sample_n+"_eta_barrel_pass","",N_eta_B, eta_low, eta_hight);
H_eta_endcap_pass = new TH1F(sample_n+"_eta_endcap_pass","",N_eta_E, eta_low, eta_hight);
H_eta_transition_pass = new TH1F(sample_n+"_eta_transition_pass","",N_eta_T, eta_low, eta_hight);
H_eta_barrel_fail = new TH1F(sample_n+"_eta_barrel_fail","",N_eta_B, eta_low, eta_hight);
H_eta_endcap_fail = new TH1F(sample_n+"_eta_endcap_fail","",N_eta_E, eta_low, eta_hight);
H_eta_transition_fail = new TH1F(sample_n+"_eta_transition_fail","",N_eta_T, eta_low, eta_hight);
int N_phi_B=100;
int N_phi_E=100;
int N_phi_T=100;
float phi_low=-5;
float phi_hight=5;
H_phi_barrel_all = new TH1F(sample_n+"_phi_barrel_all","",N_phi_B, phi_low, phi_hight);
H_phi_endcap_all = new TH1F(sample_n+"_phi_endcap_all","",N_phi_E, phi_low, phi_hight);
H_phi_transition_all = new TH1F(sample_n+"_phi_transition_all","",N_phi_T, phi_low, phi_hight);
H_phi_barrel_pass = new TH1F(sample_n+"_phi_barrel_pass","",N_phi_B, phi_low, phi_hight);
H_phi_endcap_pass = new TH1F(sample_n+"_phi_endcap_pass","",N_phi_E, phi_low, phi_hight);
H_phi_transition_pass = new TH1F(sample_n+"_phi_transition_pass","",N_phi_T, phi_low, phi_hight);
H_phi_barrel_fail = new TH1F(sample_n+"_phi_barrel_fail","",N_phi_B, phi_low, phi_hight);
H_phi_endcap_fail = new TH1F(sample_n+"_phi_endcap_fail","",N_phi_E, phi_low, phi_hight);
H_phi_transition_fail = new TH1F(sample_n+"_phi_transition_fail","",N_phi_T, phi_low, phi_hight);
//H_Et_barrel_all = new TH1F(sample_n+"_Et_barrel_all","",30, Et_barrel);
//H_Et_endcap_all = new TH1F(sample_n+"_Et_endcap_all","",12, Et_endcap);
//H_Et_transition_all = new TH1F(sample_n+"_Et_transition_all","",3, Et_transition);
//
//H_Et_barrel_pass = new TH1F(sample_n+"_Et_barrel_pass","",30, Et_barrel);
//H_Et_endcap_pass = new TH1F(sample_n+"_Et_endcap_pass","",12, Et_endcap);
//H_Et_transition_pass = new TH1F(sample_n+"_Et_transition_pass","",3, Et_transition);
//
//H_Et_barrel_fail = new TH1F(sample_n+"_Et_barrel_fail","",30, Et_barrel);
//H_Et_endcap_fail = new TH1F(sample_n+"_Et_endcap_fail","",12, Et_endcap);
//H_Et_transition_fail = new TH1F(sample_n+"_Et_transition_fail","",3, Et_transition);
int N_bin_RePt=300;
float RePt_low=0;
float RePt_high=300;
H_RePt_barrel_all = new TH1F(sample_n+"_RePt_barrel_all","", N_bin_RePt, RePt_low, RePt_high); 
H_RePt_endcap_all = new TH1F(sample_n+"_RePt_endcap_all","", N_bin_RePt, RePt_low, RePt_high); 
H_RePt_transition_all = new TH1F(sample_n+"_RePt_transition_all","", N_bin_RePt, RePt_low, RePt_high); 
H_RePt_barrel_pass = new TH1F(sample_n+"_RePt_barrel_pass","", N_bin_RePt, RePt_low, RePt_high); 
H_RePt_endcap_pass = new TH1F(sample_n+"_RePt_endcap_pass","", N_bin_RePt, RePt_low, RePt_high); 
H_RePt_transition_pass = new TH1F(sample_n+"_RePt_transition_pass","", N_bin_RePt, RePt_low, RePt_high); 
H_RePt_barrel_fail = new TH1F(sample_n+"_RePt_barrel_fail","", N_bin_RePt, RePt_low, RePt_high); 
H_RePt_endcap_fail = new TH1F(sample_n+"_RePt_endcap_fail","", N_bin_RePt, RePt_low, RePt_high); 
H_RePt_transition_fail = new TH1F(sample_n+"_RePt_transition_fail","", N_bin_RePt, RePt_low, RePt_high); 

H_RePt_BE_all = new TH1F(sample_n+"_RePt_BE_all","",   N_bin_RePt, RePt_low, RePt_high); 
H_RePt_BE_pass = new TH1F(sample_n+"_RePt_BE_pass","", N_bin_RePt, RePt_low, RePt_high); 
H_RePt_BE_fail = new TH1F(sample_n+"_RePt_BE_fail","", N_bin_RePt, RePt_low, RePt_high); 

int N_bin_Mee=80;
float Mee_low=70;
float Mee_high=110;
H_Mee_barrel_all = new TH1F(sample_n+"_Mee_barrel_all","", N_bin_Mee, Mee_low, Mee_high); 
H_Mee_endcap_all = new TH1F(sample_n+"_Mee_endcap_all","", N_bin_Mee, Mee_low, Mee_high); 
H_Mee_transition_all = new TH1F(sample_n+"_Mee_transition_all","", N_bin_Mee, Mee_low, Mee_high); 
H_Mee_barrel_pass = new TH1F(sample_n+"_Mee_barrel_pass","", N_bin_Mee, Mee_low, Mee_high); 
H_Mee_endcap_pass = new TH1F(sample_n+"_Mee_endcap_pass","", N_bin_Mee, Mee_low, Mee_high); 
H_Mee_transition_pass = new TH1F(sample_n+"_Mee_transition_pass","", N_bin_Mee, Mee_low, Mee_high); 
H_Mee_barrel_fail = new TH1F(sample_n+"_Mee_barrel_fail","", N_bin_Mee, Mee_low, Mee_high); 
H_Mee_endcap_fail = new TH1F(sample_n+"_Mee_endcap_fail","", N_bin_Mee, Mee_low, Mee_high); 
H_Mee_transition_fail = new TH1F(sample_n+"_Mee_transition_fail","", N_bin_Mee, Mee_low, Mee_high); 

}
void Fill_plots(){
TH1::SetDefaultSumw2();
float t_Et;
float t_eta;
float t_phi;
float p_Et;
float p_eta;
float p_phi;
Int_t p_region;
Int_t p_ID_nominal;
float w_PU_combined;
float mee;
float Pt=0;
int t_PreSelection;
int p_PreSelection;
int t_passtrigger;
int p_passtrigger;
TChain *ch = new TChain(chain_n);
ch->Add(file_n);
ch->SetBranchAddress("t_Et",&t_Et);
ch->SetBranchAddress("t_eta",&t_eta);
ch->SetBranchAddress("t_phi",&t_phi);
ch->SetBranchAddress("p_Et",&p_Et);
ch->SetBranchAddress("p_eta",&p_eta);
ch->SetBranchAddress("p_phi",&p_phi);
ch->SetBranchAddress("p_region",&p_region);
ch->SetBranchAddress("p_ID_nominal",&p_ID_nominal);
ch->SetBranchAddress("w_PU_combined",&w_PU_combined);
ch->SetBranchAddress("mee",&mee);
if(sample_n=="data_silver" || sample_n=="ZToEE"){
ch->SetBranchAddress("Pt",&Pt);
}
if( do_fake_rate) {
ch->SetBranchAddress("t_PreSelection",&t_PreSelection);
ch->SetBranchAddress("t_passtrigger",&t_passtrigger);
ch->SetBranchAddress("p_PreSelection",&p_PreSelection);
ch->SetBranchAddress("p_passtrigger",&p_passtrigger);
}
std::cout<<sample_n<<" :Entries= "<<ch->GetEntries()<<std::endl;

float t_fake_rate=1;
float p_fake_rate=1;
int N_feve=0;
int N_fill=0;
float f_w_PU_combined=0;
float f_w_sign=0;
for(long long i=0; i<ch->GetEntries(); i++){
ch->GetEntry(i);
Int_t w_sign = w_PU_combined < 0 ? -1 : 1 ;

if(t_Et<35 || p_Et<35 || mee>110 || mee <70) continue;
//std::cout<<"N="<<i<<std::endl;
if(doPUW==true){
H_tag_Et->Fill(t_Et,w_PU_combined);
H_tag_Eta->Fill(t_eta,w_PU_combined);
H_tag_Phi->Fill(t_phi,w_PU_combined);}
else{
H_tag_Et->Fill(t_Et,w_sign);
H_tag_Eta->Fill(t_eta,w_sign);
H_tag_Phi->Fill(t_phi,w_sign);}
if(do_fake_rate==false){
if(p_region==1) {if(doPUW==true) {
                                  H_Et_barrel_all->Fill(p_Et,w_PU_combined);
                                  H_RePt_barrel_all->Fill(Pt,w_PU_combined);
                                  H_Mee_barrel_all->Fill(mee,w_PU_combined);
                                  H_eta_barrel_all->Fill(p_eta,w_PU_combined);
                                  H_phi_barrel_all->Fill(p_phi,w_PU_combined);
                                 }  
                 else {
                       H_Et_barrel_all->Fill(p_Et, w_sign);
                       H_RePt_barrel_all->Fill(Pt, w_sign);
                       H_Mee_barrel_all->Fill(mee, w_sign);
                       H_eta_barrel_all->Fill(p_eta,w_sign);
                       H_phi_barrel_all->Fill(p_phi,w_sign);
                      }
                }
else if(p_region==2) {if(doPUW==true) {
                                       H_Et_transition_all->Fill(p_Et,w_PU_combined);
                                       H_RePt_transition_all->Fill(Pt,w_PU_combined);
                                       H_Mee_transition_all->Fill(mee,w_PU_combined);
                                       H_eta_transition_all->Fill(p_eta,w_PU_combined);
                                       H_phi_transition_all->Fill(p_phi,w_PU_combined);
                                      }
                 else {
                       H_Et_transition_all->Fill(p_Et, w_sign);
                       H_RePt_transition_all->Fill(Pt, w_sign);
                       H_Mee_transition_all->Fill(mee, w_sign);
                       H_eta_transition_all->Fill(p_eta,w_sign);
                       H_phi_transition_all->Fill(p_phi,w_sign);
                      }
                } 
else if(p_region==3) {if(doPUW==true) {
                                       H_Et_endcap_all->Fill(p_Et,w_PU_combined);
                                       H_RePt_endcap_all->Fill(Pt,w_PU_combined);
                                       H_Mee_endcap_all->Fill(mee,w_PU_combined);
                                       H_eta_endcap_all->Fill(p_eta,w_PU_combined);
                                       H_phi_endcap_all->Fill(p_phi,w_PU_combined);
                                       }
                 else {
                       H_Et_endcap_all->Fill(p_Et, w_sign);
                       H_RePt_endcap_all->Fill(Pt, w_sign);
                       H_Mee_endcap_all->Fill(mee, w_sign);
                       H_eta_endcap_all->Fill(p_eta,w_sign);
                       H_phi_endcap_all->Fill(p_phi,w_sign);
                      }
                }
else {N_out++;}
}
if(do_fake_rate==false && p_ID_nominal==1) {

if(p_region==1) {if(doPUW==true) {
                                  H_Et_barrel_pass->Fill(p_Et,w_PU_combined);
                                  H_RePt_barrel_pass->Fill(Pt,w_PU_combined);
                                  H_Mee_barrel_pass->Fill(mee,w_PU_combined);
                                  H_eta_barrel_pass->Fill(p_eta,w_PU_combined);
                                  H_phi_barrel_pass->Fill(p_phi,w_PU_combined);
                                 }  
                 else {
                       H_Et_barrel_pass->Fill(p_Et, w_sign);
                       H_RePt_barrel_pass->Fill(Pt, w_sign);
                       H_Mee_barrel_pass->Fill(mee, w_sign);
                       H_eta_barrel_pass->Fill(p_eta,w_sign);
                       H_phi_barrel_pass->Fill(p_phi,w_sign);
                      }
                }
else if(p_region==2) {if(doPUW==true) {
                                       H_Et_transition_pass->Fill(p_Et,w_PU_combined);
                                       H_RePt_transition_pass->Fill(Pt,w_PU_combined);
                                       H_Mee_transition_pass->Fill(mee,w_PU_combined);
                                       H_eta_transition_pass->Fill(p_eta,w_PU_combined);
                                       H_phi_transition_pass->Fill(p_phi,w_PU_combined);
                                      }
                 else {
                       H_Et_transition_pass->Fill(p_Et, w_sign);
                       H_RePt_transition_pass->Fill(Pt, w_sign);
                       H_Mee_transition_pass->Fill(mee, w_sign);
                       H_eta_transition_pass->Fill(p_eta,w_sign);
                       H_phi_transition_pass->Fill(p_phi,w_sign);
                      }
                } 
else if(p_region==3) {if(doPUW==true) {
                                       H_Et_endcap_pass->Fill(p_Et,w_PU_combined);
                                       H_RePt_endcap_pass->Fill(Pt,w_PU_combined);
                                       H_Mee_endcap_pass->Fill(mee,w_PU_combined);
                                       H_eta_endcap_pass->Fill(p_eta,w_PU_combined);
                                       H_phi_endcap_pass->Fill(p_phi,w_PU_combined);
                                       }
                 else {
                       H_Et_endcap_pass->Fill(p_Et, w_sign);
                       H_RePt_endcap_pass->Fill(Pt, w_sign);
                       H_Mee_endcap_pass->Fill(mee, w_sign);
                       H_eta_endcap_pass->Fill(p_eta,w_sign);
                       H_phi_endcap_pass->Fill(p_phi,w_sign);
                      }
                }
               }


if (do_fake_rate == true || p_ID_nominal==0){

if(p_region==1) {if(doPUW==true) {
                                  H_Et_barrel_fail->Fill(p_Et,w_PU_combined);
                                  H_eta_barrel_fail->Fill(p_eta,w_PU_combined);
                                  H_phi_barrel_fail->Fill(p_phi,w_PU_combined);
                                  H_RePt_barrel_fail->Fill(Pt,w_PU_combined);
                                  H_Mee_barrel_fail->Fill(mee,w_PU_combined);
                                 }  
                 else {
                       H_Et_barrel_fail->Fill(p_Et, w_sign);
                       H_eta_barrel_fail->Fill(p_eta, w_sign);
                       H_phi_barrel_fail->Fill(p_phi, w_sign);
                       H_RePt_barrel_fail->Fill(Pt, w_sign);
                       H_Mee_barrel_fail->Fill(mee, w_sign);
                      }
                }
else if(p_region==2) {if(doPUW==true) {
                                       H_Et_transition_fail->Fill(p_Et,w_PU_combined);
                                       H_eta_transition_fail->Fill(p_eta,w_PU_combined);
                                       H_phi_transition_fail->Fill(p_phi,w_PU_combined);
                                       H_RePt_transition_fail->Fill(Pt,w_PU_combined);
                                       H_Mee_transition_fail->Fill(mee,w_PU_combined);
                                      }
                 else {
                       H_Et_transition_fail->Fill(p_Et, w_sign);
                       H_eta_transition_fail->Fill(p_eta, w_sign);
                       H_phi_transition_fail->Fill(p_phi, w_sign);
                       H_RePt_transition_fail->Fill(Pt, w_sign);
                       H_Mee_transition_fail->Fill(mee, w_sign);
                      }
                } 
else if(p_region==3) {if(doPUW==true) {
                                       H_Et_endcap_fail->Fill(p_Et,w_PU_combined);
                                       H_eta_endcap_fail->Fill(p_eta,w_PU_combined);
                                       H_phi_endcap_fail->Fill(p_phi,w_PU_combined);
                                       H_RePt_endcap_fail->Fill(Pt,w_PU_combined);
                                       H_Mee_endcap_fail->Fill(mee,w_PU_combined);
                                       }
                 else {
                       H_Et_endcap_fail->Fill(p_Et, w_sign);
                       H_eta_endcap_fail->Fill(p_eta, w_sign);
                       H_phi_endcap_fail->Fill(p_phi, w_sign);
                       H_RePt_endcap_fail->Fill(Pt, w_sign);
                       H_Mee_endcap_fail->Fill(mee, w_sign);
                      }
                }

N_fill++;
               }

N_feve++;

}//for
std::cout<<"feve"<<N_feve<<std::endl;
std::cout<<"fill"<<N_fill<<std::endl;
if(do_fake_rate == true){
H_Et_barrel_all->Add(H_Et_barrel_fail,H_Et_barrel_pass,1,1);
H_Et_endcap_all->Add(H_Et_endcap_fail,H_Et_endcap_pass,1,1);
H_Et_transition_all->Add(H_Et_transition_fail,H_Et_transition_pass,1,1);

H_RePt_barrel_all->Add(H_RePt_barrel_fail,H_RePt_barrel_pass,1,1);
H_RePt_endcap_all->Add(H_RePt_endcap_fail,H_RePt_endcap_pass,1,1);
H_RePt_transition_all->Add(H_RePt_transition_fail,H_RePt_transition_pass,1,1);
//H_RePt_BE_all->Add(H_RePt_barrel_fail,H_RePt_endcap_all,1,1); 
//H_RePt_BE_pass->Add(H_RePt_barrel_fail,H_RePt_endcap_pass,1,1); 
//H_RePt_BE_all->Add(H_RePt_barrel_fail,H_RePt_endcap_all,1,1); 

H_Mee_barrel_all->Add(H_Mee_barrel_fail,H_Mee_barrel_pass,1,1);
H_Mee_endcap_all->Add(H_Mee_endcap_fail,H_Mee_endcap_pass,1,1);
H_Mee_transition_all->Add(H_Mee_transition_fail,H_Mee_transition_pass,1,1);
}
//H_Et_barrel_all->Draw();
if(isData==false){

H_tag_Et->Scale(scale_to_data);
H_tag_Eta->Scale(scale_to_data);
H_tag_Phi->Scale(scale_to_data);
 
H_Et_barrel_all->Scale(scale_to_data);
H_Et_barrel_pass->Scale(scale_to_data);
H_Et_barrel_fail->Scale(scale_to_data);
H_Et_endcap_all->Scale(scale_to_data);
H_Et_endcap_pass->Scale(scale_to_data);
H_Et_endcap_fail->Scale(scale_to_data);
H_Et_transition_all->Scale(scale_to_data);
H_Et_transition_pass->Scale(scale_to_data);
H_Et_transition_fail->Scale(scale_to_data);

H_eta_barrel_all->Scale(scale_to_data);
H_eta_barrel_pass->Scale(scale_to_data);
H_eta_barrel_fail->Scale(scale_to_data);
H_eta_endcap_all->Scale(scale_to_data);
H_eta_endcap_pass->Scale(scale_to_data);
H_eta_endcap_fail->Scale(scale_to_data);
H_eta_transition_all->Scale(scale_to_data);
H_eta_transition_pass->Scale(scale_to_data);
H_eta_transition_fail->Scale(scale_to_data);
H_phi_barrel_all->Scale(scale_to_data);
H_phi_barrel_pass->Scale(scale_to_data);
H_phi_barrel_fail->Scale(scale_to_data);
H_phi_endcap_all->Scale(scale_to_data);
H_phi_endcap_pass->Scale(scale_to_data);
H_phi_endcap_fail->Scale(scale_to_data);
H_phi_transition_all->Scale(scale_to_data);
H_phi_transition_pass->Scale(scale_to_data);
H_phi_transition_fail->Scale(scale_to_data);
H_RePt_barrel_all->Scale(scale_to_data);
H_RePt_barrel_pass->Scale(scale_to_data);
H_RePt_barrel_fail->Scale(scale_to_data);
H_RePt_endcap_all->Scale(scale_to_data);
H_RePt_endcap_pass->Scale(scale_to_data);
H_RePt_endcap_fail->Scale(scale_to_data);
H_RePt_transition_all->Scale(scale_to_data);
H_RePt_transition_pass->Scale(scale_to_data);
H_RePt_transition_fail->Scale(scale_to_data);
H_RePt_BE_all->Scale(scale_to_data);
H_RePt_BE_pass->Scale(scale_to_data);
H_RePt_BE_fail->Scale(scale_to_data);

H_Mee_barrel_all->Scale(scale_to_data);
H_Mee_barrel_pass->Scale(scale_to_data);
H_Mee_barrel_fail->Scale(scale_to_data);
H_Mee_endcap_all->Scale(scale_to_data);
H_Mee_endcap_pass->Scale(scale_to_data);
H_Mee_endcap_fail->Scale(scale_to_data);
H_Mee_transition_all->Scale(scale_to_data);
H_Mee_transition_pass->Scale(scale_to_data);
H_Mee_transition_fail->Scale(scale_to_data);
}
//H_Et_barrel_all->Draw();
}//void Fill_plots()

};

string cvt(const float f, const int prec);


void draw_Hstack(TH1F *h_ZToEE, TH1F *h_ZToTT, TH1F *h_WW, TH1F *h_WJets, TH1F *h_ttbar, TH1F *h_ST, TH1F *h_ST_anti, TH1F* h_gJ1, TH1F* h_gJ2,TH1F* h_gJ3,TH1F* h_gJ4,TH1F* h_gJ5 ,TH1F *h_data,TString var_name, TString region_name, TString pass_name, TString PUW_name);

void draw_Pt(TH1F *h_data, TH1F *h_mc, TString var_name, TString region_name, TString pass_name, TString PUW_name, TString outfile_name);
void plot_kinematic(){

gStyle->SetOptStat(0);
TH1::SetDefaultSumw2();
bool plot_fail_fail=true;

File_plots *data_silver = new File_plots("data_silver","./ntuples/reskim/data_silver_DBE_fakeRate_noTrig.root","tap", -1,       -1,      true,  true, plot_fail_fail);
//File_plots *data_silver = new File_plots("data_silver","./ntuples/reskim/data_silver_EndcapCorr.root","tap", -1,       -1,      true,  true, false);
File_plots *ZToEE       = new File_plots("ZToEE",      "./ntuples/reskim/ZToEE_powheg_fakeRate_noTrig.root",      "tap", 2857302,  1975,    false, true ,plot_fail_fail);
//File_plots *ZToEE       = new File_plots("ZToEE",      "./ntuples/reskim/ZToEE_powheg_Pt_weighted.root","tap", 2857302,  1975,    false, true);
File_plots *ZToTT       = new File_plots("ZToTT",      "./ntuples/reskim/ZToTT_amc_fakeRate_noTrig.root",      "tap", 20373245, 6025.2,  false, true, plot_fail_fail);
File_plots *WW          = new File_plots("WW",         "./ntuples/reskim/WW_fakeRate_noTrig.root",         "tap", 988418,   118.7,   false, true, plot_fail_fail);
File_plots *WJets       = new File_plots("WJets",      "./ntuples/reskim/WJets_madgraph_fakeRate_noTrig.root",      "tap", 46893463, 61526.7, false, true, plot_fail_fail);
File_plots *ttbar       = new File_plots("ttbar",      "./ntuples/reskim/ttbar_fakeRate_noTrig.root",      "tap", 97784324, 831.76,  false, true, plot_fail_fail);
File_plots *ST          = new File_plots("ST",         "./ntuples/reskim/ST_fakeRate_noTrig.root",         "tap", 999400,   35.6,    false, true, plot_fail_fail);
File_plots *ST_anti     = new File_plots("ST_anti",    "./ntuples/reskim/ST_anti_fakeRate_noTrig.root",    "tap", 1000000,  35.6,    false, true, plot_fail_fail);
File_plots *GamJet1     = new File_plots("GamJet1",    "./ntuples/reskim/GamJet_40_100_FR.root",           "tap", 4424830,  20790,    false, true, plot_fail_fail);
File_plots *GamJet2     = new File_plots("GamJet2",    "./ntuples/reskim/GamJet_100_200_FR.root",          "tap", 5116711,  9238,    false, true, plot_fail_fail);
File_plots *GamJet3     = new File_plots("GamJet3",    "./ntuples/reskim/GamJet_200_400_FR.root",          "tap", 10490427, 2305,    false, true, plot_fail_fail);
File_plots *GamJet4     = new File_plots("GamJet4",    "./ntuples/reskim/GamJet_400_600_FR.root",          "tap", 2356919 , 274.4 ,    false, true, plot_fail_fail);
File_plots *GamJet5     = new File_plots("GamJet5",    "./ntuples/reskim/GamJet_600_Inf_FR.root",          "tap", 2456253 , 93.46 ,    false, true, plot_fail_fail);
data_silver->Fill_plots();
ZToEE->Fill_plots();
ZToTT->Fill_plots();
WW->Fill_plots();
WJets->Fill_plots();
ttbar->Fill_plots();
ST->Fill_plots();
ST_anti->Fill_plots();
GamJet1->Fill_plots();
GamJet2->Fill_plots();
GamJet3->Fill_plots();
GamJet4->Fill_plots();
GamJet5->Fill_plots();
std::cout<<"data barrel all"<<data_silver->H_Et_barrel_all->Integral()<<std::endl;
std::cout<<"data barrel pass"<<data_silver->H_Et_barrel_pass->Integral()<<std::endl;
std::cout<<"data barrel fail"<<data_silver->H_Et_barrel_fail->Integral()<<std::endl;
std::cout<<"data endcap all"<<data_silver->H_Et_endcap_all->Integral()<<std::endl;
std::cout<<"data endcap pass"<<data_silver->H_Et_endcap_pass->Integral()<<std::endl;
std::cout<<"data endcap fail"<<data_silver->H_Et_endcap_fail->Integral()<<std::endl;

draw_Hstack(ZToEE->H_tag_Et, ZToTT->H_tag_Et, WW->H_tag_Et, WJets->H_tag_Et, ttbar->H_tag_Et, ST->H_tag_Et, ST_anti->H_tag_Et,GamJet1->H_tag_Et, GamJet2->H_tag_Et, GamJet3->H_tag_Et,GamJet4->H_tag_Et,GamJet5->H_tag_Et,data_silver->H_tag_Et, "Et","barrel","tag","PUW");
draw_Hstack(ZToEE->H_tag_Eta, ZToTT->H_tag_Eta, WW->H_tag_Eta, WJets->H_tag_Eta, ttbar->H_tag_Eta, ST->H_tag_Eta, ST_anti->H_tag_Eta,GamJet1->H_tag_Eta, GamJet2->H_tag_Eta, GamJet3->H_tag_Eta,GamJet4->H_tag_Eta,GamJet5->H_tag_Eta,data_silver->H_tag_Eta, "eta","barrel","tag","PUW");
draw_Hstack(ZToEE->H_tag_Phi, ZToTT->H_tag_Phi, WW->H_tag_Phi, WJets->H_tag_Phi, ttbar->H_tag_Phi, ST->H_tag_Phi, ST_anti->H_tag_Phi,GamJet1->H_tag_Phi, GamJet2->H_tag_Phi, GamJet3->H_tag_Phi,GamJet4->H_tag_Phi,GamJet5->H_tag_Phi,data_silver->H_tag_Phi, "phi","barrel","tag","PUW");



draw_Hstack(ZToEE->H_Et_barrel_fail, ZToTT->H_Et_barrel_fail, WW->H_Et_barrel_fail, WJets->H_Et_barrel_fail, ttbar->H_Et_barrel_fail, ST->H_Et_barrel_fail, ST_anti->H_Et_barrel_fail,GamJet1->H_Et_barrel_fail,GamJet2->H_Et_barrel_fail, GamJet3->H_Et_barrel_fail,GamJet4->H_Et_barrel_fail,GamJet5->H_Et_barrel_fail,data_silver->H_Et_barrel_fail, "Et","barrel","fail","PUW");
draw_Hstack(ZToEE->H_eta_barrel_fail, ZToTT->H_eta_barrel_fail, WW->H_eta_barrel_fail, WJets->H_eta_barrel_fail, ttbar->H_eta_barrel_fail, ST->H_eta_barrel_fail, ST_anti->H_eta_barrel_fail,GamJet1->H_eta_barrel_fail,GamJet2->H_eta_barrel_fail, GamJet3->H_eta_barrel_fail,GamJet4->H_eta_barrel_fail,GamJet5->H_eta_barrel_fail,data_silver->H_eta_barrel_fail, "eta","barrel","fail","PUW");
draw_Hstack(ZToEE->H_phi_barrel_fail, ZToTT->H_phi_barrel_fail, WW->H_phi_barrel_fail, WJets->H_phi_barrel_fail, ttbar->H_phi_barrel_fail, ST->H_phi_barrel_fail, ST_anti->H_phi_barrel_fail,GamJet1->H_phi_barrel_fail,GamJet2->H_phi_barrel_fail, GamJet3->H_phi_barrel_fail,GamJet4->H_phi_barrel_fail,GamJet5->H_phi_barrel_fail,data_silver->H_phi_barrel_fail, "phi","barrel","fail","PUW");

draw_Hstack(ZToEE->H_Et_endcap_fail, ZToTT->H_Et_endcap_fail, WW->H_Et_endcap_fail, WJets->H_Et_endcap_fail, ttbar->H_Et_endcap_fail, ST->H_Et_endcap_fail, ST_anti->H_Et_endcap_fail,GamJet1->H_Et_endcap_fail,GamJet2->H_Et_endcap_fail, GamJet3->H_Et_endcap_fail,GamJet4->H_Et_endcap_fail,GamJet5->H_Et_endcap_fail,data_silver->H_Et_endcap_fail, "Et","endcap","fail","PUW");
draw_Hstack(ZToEE->H_eta_endcap_fail, ZToTT->H_eta_endcap_fail, WW->H_eta_endcap_fail, WJets->H_eta_endcap_fail, ttbar->H_eta_endcap_fail, ST->H_eta_endcap_fail, ST_anti->H_eta_endcap_fail,GamJet1->H_eta_endcap_fail,GamJet2->H_eta_endcap_fail, GamJet3->H_eta_endcap_fail,GamJet4->H_eta_endcap_fail,GamJet5->H_eta_endcap_fail,data_silver->H_eta_endcap_fail, "eta","endcap","fail","PUW");
draw_Hstack(ZToEE->H_phi_endcap_fail, ZToTT->H_phi_endcap_fail, WW->H_phi_endcap_fail, WJets->H_phi_endcap_fail, ttbar->H_phi_endcap_fail, ST->H_phi_endcap_fail, ST_anti->H_phi_endcap_fail,GamJet1->H_phi_endcap_fail,GamJet2->H_phi_endcap_fail, GamJet3->H_phi_endcap_fail,GamJet4->H_phi_endcap_fail,GamJet5->H_phi_endcap_fail,data_silver->H_phi_endcap_fail, "phi","endcap","fail","PUW");

draw_Hstack(ZToEE->H_Mee_barrel_fail, ZToTT->H_Mee_barrel_fail, WW->H_Mee_barrel_fail, WJets->H_Mee_barrel_fail, ttbar->H_Mee_barrel_fail, ST->H_Mee_barrel_fail, ST_anti->H_Mee_barrel_fail,GamJet1->H_Mee_barrel_fail,GamJet2->H_Mee_barrel_fail, GamJet3->H_Mee_barrel_fail,GamJet4->H_Mee_barrel_fail,GamJet5->H_Mee_barrel_fail,data_silver->H_Mee_barrel_fail, "Mee","barrel","fail","PUW");

draw_Hstack(ZToEE->H_Mee_endcap_fail, ZToTT->H_Mee_endcap_fail, WW->H_Mee_endcap_fail, WJets->H_Mee_endcap_fail, ttbar->H_Mee_endcap_fail, ST->H_Mee_endcap_fail, ST_anti->H_Mee_endcap_fail,GamJet1->H_Mee_endcap_fail,GamJet2->H_Mee_endcap_fail, GamJet3->H_Mee_endcap_fail,GamJet4->H_Mee_endcap_fail,GamJet5->H_Mee_endcap_fail,data_silver->H_Mee_endcap_fail, "Mee","endcap","fail","PUW");


/*
draw_Hstack(ZToEE->H_Et_barrel_pass, ZToTT->H_Et_barrel_pass, WW->H_Et_barrel_pass, WJets->H_Et_barrel_pass, ttbar->H_Et_barrel_pass, ST->H_Et_barrel_pass, ST_anti->H_Et_barrel_pass,GamJet1->H_Et_barrel_pass,GamJet2->H_Et_barrel_pass, GamJet3->H_Et_barrel_pass,GamJet4->H_Et_barrel_pass,GamJet5->H_Et_barrel_pass,data_silver->H_Et_barrel_pass, "Et","barrel","pass","PUW");
draw_Hstack(ZToEE->H_eta_barrel_pass, ZToTT->H_eta_barrel_pass, WW->H_eta_barrel_pass, WJets->H_eta_barrel_pass, ttbar->H_eta_barrel_pass, ST->H_eta_barrel_pass, ST_anti->H_eta_barrel_pass,GamJet1->H_eta_barrel_pass,GamJet2->H_eta_barrel_pass, GamJet3->H_eta_barrel_pass,GamJet4->H_eta_barrel_pass,GamJet5->H_eta_barrel_pass,data_silver->H_eta_barrel_pass, "eta","barrel","pass","PUW");
draw_Hstack(ZToEE->H_phi_barrel_pass, ZToTT->H_phi_barrel_pass, WW->H_phi_barrel_pass, WJets->H_phi_barrel_pass, ttbar->H_phi_barrel_pass, ST->H_phi_barrel_pass, ST_anti->H_phi_barrel_pass,GamJet1->H_phi_barrel_pass,GamJet2->H_phi_barrel_pass, GamJet3->H_phi_barrel_pass,GamJet4->H_phi_barrel_pass,GamJet5->H_phi_barrel_pass,data_silver->H_phi_barrel_pass, "phi","barrel","pass","PUW");
draw_Hstack(ZToEE->H_Mee_barrel_pass, ZToTT->H_Mee_barrel_pass, WW->H_Mee_barrel_pass, WJets->H_Mee_barrel_pass, ttbar->H_Mee_barrel_pass, ST->H_Mee_barrel_pass, ST_anti->H_Mee_barrel_pass,GamJet1->H_Mee_barrel_pass,GamJet2->H_Mee_barrel_pass, GamJet3->H_Mee_barrel_pass,GamJet4->H_Mee_barrel_pass,GamJet5->H_Mee_barrel_pass,data_silver->H_Mee_barrel_pass, "Mee","barrel","pass","PUW");

draw_Hstack(ZToEE->H_Et_endcap_pass, ZToTT->H_Et_endcap_pass, WW->H_Et_endcap_pass, WJets->H_Et_endcap_pass, ttbar->H_Et_endcap_pass, ST->H_Et_endcap_pass, ST_anti->H_Et_endcap_pass,GamJet1->H_Et_endcap_pass,GamJet2->H_Et_endcap_pass, GamJet3->H_Et_endcap_pass,GamJet4->H_Et_endcap_pass,GamJet5->H_Et_endcap_pass,data_silver->H_Et_endcap_pass, "Et","endcap","pass","PUW");
draw_Hstack(ZToEE->H_eta_endcap_pass, ZToTT->H_eta_endcap_pass, WW->H_eta_endcap_pass, WJets->H_eta_endcap_pass, ttbar->H_eta_endcap_pass, ST->H_eta_endcap_pass, ST_anti->H_eta_endcap_pass,GamJet1->H_eta_endcap_pass,GamJet2->H_eta_endcap_pass, GamJet3->H_eta_endcap_pass,GamJet4->H_eta_endcap_pass,GamJet5->H_eta_endcap_pass,data_silver->H_eta_endcap_pass, "eta","endcap","pass","PUW");
draw_Hstack(ZToEE->H_phi_endcap_pass, ZToTT->H_phi_endcap_pass, WW->H_phi_endcap_pass, WJets->H_phi_endcap_pass, ttbar->H_phi_endcap_pass, ST->H_phi_endcap_pass, ST_anti->H_phi_endcap_pass,GamJet1->H_phi_endcap_pass,GamJet2->H_phi_endcap_pass, GamJet3->H_phi_endcap_pass,GamJet4->H_phi_endcap_pass,GamJet5->H_phi_endcap_pass,data_silver->H_phi_endcap_pass, "phi","endcap","pass","PUW");
draw_Hstack(ZToEE->H_Mee_endcap_pass, ZToTT->H_Mee_endcap_pass, WW->H_Mee_endcap_pass, WJets->H_Mee_endcap_pass, ttbar->H_Mee_endcap_pass, ST->H_Mee_endcap_pass, ST_anti->H_Mee_endcap_pass,GamJet1->H_Mee_endcap_pass,GamJet2->H_Mee_endcap_pass, GamJet3->H_Mee_endcap_pass,GamJet4->H_Mee_endcap_pass,GamJet5->H_Mee_endcap_pass,data_silver->H_Mee_endcap_pass, "Mee","endcap","pass","PUW");
*/
/*
//TString out_file = "Pt_ratio.root";
TString out_file = "Pt_ratio_after_Ptweight.root";
draw_Pt(data_silver->H_RePt_barrel_all, ZToEE->H_RePt_barrel_all, "RePt", "barrel", "all", "PUW", out_file);
draw_Pt(data_silver->H_RePt_barrel_pass, ZToEE->H_RePt_barrel_pass, "RePt", "barrel", "pass", "PUW", out_file);
draw_Pt(data_silver->H_RePt_barrel_fail, ZToEE->H_RePt_barrel_fail, "RePt", "barrel", "fail", "PUW", out_file);
draw_Pt(data_silver->H_RePt_endcap_all, ZToEE->H_RePt_endcap_all, "RePt", "endcap", "all", "PUW", out_file);
draw_Pt(data_silver->H_RePt_endcap_pass, ZToEE->H_RePt_endcap_pass, "RePt", "endcap", "pass", "PUW", out_file);
draw_Pt(data_silver->H_RePt_endcap_fail, ZToEE->H_RePt_endcap_fail, "RePt", "endcap", "fail", "PUW", out_file);
draw_Pt(data_silver->H_RePt_BE_all, ZToEE->H_RePt_BE_all, "RePt", "BE", "all", "PUW", out_file);
draw_Pt(data_silver->H_RePt_BE_pass, ZToEE->H_RePt_BE_pass, "RePt", "BE", "pass", "PUW", out_file);
draw_Pt(data_silver->H_RePt_BE_fail, ZToEE->H_RePt_BE_fail, "RePt", "BE", "fail", "PUW", out_file);
*/
}


void draw_Hstack(TH1F *h_ZToEE, TH1F *h_ZToTT, TH1F *h_WW, TH1F *h_WJets, TH1F *h_ttbar, TH1F *h_ST, TH1F *h_ST_anti, TH1F* h_gJ1, TH1F* h_gJ2,TH1F* h_gJ3,TH1F* h_gJ4,TH1F* h_gJ5 ,TH1F *h_data,TString var_name, TString region_name, TString pass_name, TString PUW_name){
TH1::SetDefaultSumw2();
h_data->SetMarkerStyle(20);
h_data->SetMarkerColor(kBlack);

h_ZToEE->SetFillColor(kGreen);
h_ZToEE->SetMarkerStyle(21);
h_ZToEE->SetMarkerColor(kGreen);
h_ZToTT->SetFillColor(kCyan);
h_ZToTT->SetMarkerStyle(21);
h_ZToTT->SetMarkerColor(kCyan);
h_WW->SetFillColor(kMagenta);
h_WW->SetMarkerStyle(21);
h_WW->SetMarkerColor(kMagenta);
h_WJets->SetFillColor(kBlue);
h_WJets->SetMarkerStyle(21);
h_WJets->SetMarkerColor(kBlue);
h_ttbar->SetFillColor(kYellow);
h_ttbar->SetMarkerStyle(21);
h_ttbar->SetMarkerColor(kYellow);
h_ST->SetFillColor(kRed);
h_ST->SetMarkerStyle(21);
h_ST->SetMarkerColor(kRed);
h_ST_anti->SetFillColor(kOrange);
h_ST_anti->SetMarkerStyle(21);
h_ST_anti->SetMarkerColor(kOrange);
h_gJ1->SetLineColor(kCyan-10);
h_gJ1->SetFillColor(kCyan-10);
h_gJ1->SetMarkerStyle(21);
h_gJ1->SetMarkerColor(kCyan-10);
h_gJ2->SetLineColor(kCyan-10);
h_gJ2->SetFillColor(kCyan-10);
h_gJ2->SetMarkerStyle(21);
h_gJ2->SetMarkerColor(kCyan-10);
h_gJ3->SetLineColor(kCyan-10);
h_gJ3->SetFillColor(kCyan-10);
h_gJ3->SetMarkerStyle(21);
h_gJ3->SetMarkerColor(kCyan-10);
h_gJ4->SetLineColor(kCyan-10);
h_gJ4->SetFillColor(kCyan-10);
h_gJ4->SetMarkerStyle(21);
h_gJ4->SetMarkerColor(kCyan-10);
//h_gJ5->SetLineColor(kCyan-10);
h_gJ5->SetFillColor(kCyan-10);
h_gJ5->SetMarkerStyle(21);
h_gJ5->SetMarkerColor(kCyan-10);
THStack *hs = new THStack("hs","");
hs->Add(h_gJ1);
hs->Add(h_gJ2);
hs->Add(h_gJ3);
hs->Add(h_gJ4);
hs->Add(h_gJ5);
hs->Add(h_ST);
hs->Add(h_ST_anti);
hs->Add(h_WW);
hs->Add(h_WJets);
hs->Add(h_ZToTT);
hs->Add(h_ttbar);
hs->Add(h_ZToEE);

TH1F *h_sum = (TH1F*)h_ZToEE->Clone("h_sum");
h_sum->Add(h_ZToTT,1);
h_sum->Add(h_WW,1);
h_sum->Add(h_WJets,1);
h_sum->Add(h_ttbar,1);
h_sum->Add(h_ST,1);
h_sum->Add(h_ST_anti,1);
h_sum->Add(h_gJ1,1);
h_sum->Add(h_gJ2,1);
h_sum->Add(h_gJ3,1);
h_sum->Add(h_gJ4,1);
h_sum->Add(h_gJ5,1);

TCanvas *cst = new TCanvas("cst","",10,10,700,700);
cst->cd();
float size=0.3;
TPad *pad1 = new TPad("pad1", "", 0.0, size, 1.0, 1.0, 0);
TPad *pad2 = new TPad("pad2", "", 0.0, 0.0, 1.0, size, 0);
pad1->SetTopMargin(0.12);
pad1->SetLeftMargin(0.15);
pad1->SetBottomMargin(0.001);
pad2->SetTopMargin(0.001);
pad2->SetLeftMargin(0.15);
pad2->SetBottomMargin(0.3);
pad1->SetTickx();
pad1->SetTicky();

pad1->Draw();
pad2->SetTickx();
pad2->SetTicky();
pad2->Draw();
pad1->cd();
hs->SetMaximum(15*hs->GetMaximum());
hs->SetMinimum(1);
hs->Draw("hist");
hs->GetYaxis()->SetTitle("Number");
hs->GetYaxis()->SetTitleOffset(1.4);
//h_data->Draw("pe:sames");
h_data->Draw("same");

string ratio_s = cvt(h_data->GetSumOfWeights()/h_sum->GetSumOfWeights(),4);
TPaveText* tText = new TPaveText(0.6, 0.05, 0.8, 0.25, "brNDC");
tText->SetBorderSize(0);
tText->SetFillColor(0);
tText->SetFillStyle(0);
TText *t1 = tText->AddText("Data/MC="+TString(ratio_s));
tText->Draw();
std::cout<<"Data/MC="<<ratio_s<<std::endl;
TLegend *leg = new TLegend(0.65, 0.65, 0.85, 0.85);
leg->AddEntry(h_ZToEE,"Z #rightarrow ee","f");
leg->AddEntry(h_ttbar,"ttbar","f");
leg->AddEntry(h_ZToTT,"Z #rightarrow #tau#tau","f");
leg->AddEntry(h_WJets,"WJets","f");
leg->AddEntry(h_WW,"WW","f");
leg->AddEntry(h_ST_anti,"anti_ST","f");
leg->AddEntry(h_ST,"ST","f");
//leg->AddEntry(h_gJ5,"#gamma+Jet HT600-Inf","f");
//leg->AddEntry(h_gJ4,"#gamma+Jet HT400-600","f");
//leg->AddEntry(h_gJ3,"#gamma+Jet HT200-400","f");
//leg->AddEntry(h_gJ2,"#gamma+Jet HT100-200","f");
leg->AddEntry(h_gJ5,"#gamma+Jet","f");
//leg->SetMargin(0.3);
leg->SetFillColor(0);
leg->SetFillStyle(10);
leg->SetLineColor(0);
//leg->SetTextSize(0.03);
leg->Draw();

pad2->cd();
TH1F *h_ratio=(TH1F*)h_data->Clone("ratio");
h_ratio->Divide(h_sum);
h_ratio->SetMaximum(1.5);
h_ratio->SetMinimum(0.5);
h_ratio->GetXaxis()->SetLabelSize(0.08);
h_ratio->GetXaxis()->SetTitleSize(0.1);
h_ratio->GetXaxis()->SetTitleOffset(1.4);
h_ratio->GetYaxis()->SetLabelSize(0.08);
h_ratio->GetYaxis()->SetTitleSize(0.1);
h_ratio->GetYaxis()->SetTitleOffset(0.5);
h_ratio->GetYaxis()->SetTitle("Data/MC");
if(var_name=="Et") h_ratio->GetXaxis()->SetTitle("E_{T} (GeV)");
else if(var_name=="eta") h_ratio->GetXaxis()->SetTitle("#eta");
else if(var_name=="phi") h_ratio->GetXaxis()->SetTitle("#phi");
else if(var_name=="Mee") h_ratio->GetXaxis()->SetTitle("m(ee) / (GeV/c^{2})");
                   
h_ratio->Draw();
TLine *line = new TLine(h_ratio->GetXaxis()->GetXmin(), 1.0, h_ratio->GetXaxis()->GetXmax(), 1.0);
line->SetLineColor(kRed);
line->SetLineStyle(kDashed);
line->Draw();
cst->Print("my_plots/"+Date+"/"+var_name+"_"+region_name+"_"+pass_name+"_"+PUW_name+".png","png");
}

void draw_Pt(TH1F *h_data, TH1F *h_mc, TString var_name, TString region_name, TString pass_name, TString PUW_name, TString outfile_name){

TCanvas *cst = new TCanvas("cst","",10,10,700,700);
cst->cd();
float size=0.3;
TPad *pad1 = new TPad("pad1", "", 0.0, size, 1.0, 1.0, 0);
TPad *pad2 = new TPad("pad2", "", 0.0, 0.0, 1.0, size, 0);
pad1->SetTopMargin(0.12);
pad1->SetLeftMargin(0.15);
pad1->SetBottomMargin(0.001);
pad2->SetTopMargin(0.001);
pad2->SetLeftMargin(0.15);
pad2->SetBottomMargin(0.3);
pad1->SetTickx();
pad1->SetTicky();
pad1->Draw();
pad2->SetTickx();
pad2->SetTicky();
pad2->Draw();
pad1->cd();
h_mc->SetFillColor(kGreen);
h_mc->Draw("hist");
h_data->Draw("sames:pe");
string ratio_s = cvt(h_data->GetSumOfWeights()/h_mc->GetSumOfWeights(),4);
TPaveText* tText = new TPaveText(0.4, 0.5, 0.8, 0.8, "brNDC");
tText->SetBorderSize(0);
tText->SetFillColor(0);
tText->SetFillStyle(0);
TText *t1 = tText->AddText("Data/MC="+TString(ratio_s));
tText->Draw();
std::cout<<"Data/MC="<<ratio_s<<std::endl;
pad2->cd();
TH1F *h_ratio=(TH1F*)h_data->Clone("ratio_"+var_name+"_"+region_name+"_"+pass_name+"_"+PUW_name);
h_ratio->Divide(h_mc);
h_ratio->SetMaximum(1.4);
h_ratio->SetMinimum(0.6);
h_ratio->Draw();
cst->Print("my_plots/"+Date+"/"+var_name+"_"+region_name+"_"+pass_name+"_"+PUW_name+".png","png");
TFile f(outfile_name,"UPDATE");
h_ratio->Write("",TObject::kOverwrite);
f.Close();
}

string cvt(const float f, const int prec)
{
stringstream ss;
ss.precision(prec);
ss << f;
return ss.str();
}
