void fill_all_histograms_fakeRate(){
  fill_histograms_fakeRate fh ;
  
  bool do_data = true ;
  bool do_MC   = false ;
  
  if(do_data){

/*    TFile f_data_golden2015BC50ns("ntuples/reskim/data_golden2015BC50ns.root") ;
    TTree* t_data_golden2015BC50ns = (TTree*) f_data_golden2015BC50ns.Get("tap") ;
    fh.Init(t_data_golden2015BC50ns) ;
    fh.Loop("data_golden2015BC50ns") ;
    
    TFile f_data_golden2015C25ns("ntuples/reskim/data_golden2015C25ns.root") ;
    TTree* t_data_golden2015C25ns = (TTree*) f_data_golden2015C25ns.Get("tap") ;
    fh.Init(t_data_golden2015C25ns) ;
    fh.Loop("data_golden2015C25ns") ;
*/    
    TFile f_data_golden2015D("ntuples/reskim/data_golden2015D_DBE_fakeRate_noTrig.root") ;
    TTree* t_data_golden2015D = (TTree*) f_data_golden2015D.Get("tap") ;
    fh.Init(t_data_golden2015D) ;
    fh.Loop("data_golden2015D_DBE_fakeRate_noTrig") ;

    TFile f_data_silverNotGolden("ntuples/reskim/data_silverNotGolden_DBE_fakeRate_noTrig.root") ;
    TTree* t_data_silverNotGolden = (TTree*) f_data_silverNotGolden.Get("tap") ;
    fh.Init(t_data_silverNotGolden) ;
    fh.Loop("data_silverNotGolden_DBE_fakeRate_noTrig") ;

/*    
    TChain* ch_silver = new TChain("tap") ;
    ch_silver->Add("ntuples/reskim/data_golden2015D_fakeRate_noTrig.root") ;
    ch_silver->Add("ntuples/reskim/data_silverNotGolden_fakeRate_noTrig.root") ;
    fh.Init(ch_silver) ;
    fh.Loop("data_silver_fakeRateNoTrig") ;
*/
/*    TChain* ch_silver = new TChain("tap") ;
    ch_silver->Add("ntuples/reskim/data_silver_DBE_fakeRate_HEEP61.root") ;
    fh.Init(ch_silver) ;
    fh.Loop("data_silver_DBE_fakeRateNoTrig_HEEP61") ;
*/
  }
  
  if(do_MC){

    TFile f_ttbar("ntuples/reskim/ttbar_fakeRate_noTrig.root") ;
    TFile f_WW   ("ntuples/reskim/WW_fakeRate_noTrig.root"   ) ;
    TFile f_WJets("ntuples/reskim/WJets_madgraph_fakeRate_noTrig.root") ;
    TFile f_ZToEE("ntuples/reskim/ZToEE_powheg_fakeRate_noTrig.root") ;

//    TFile f_ZToEE("ntuples/reskim/ZToEE_powheg_Pt_weighted.root") ;
//    TFile f_DYToEE_mad("ntuples/reskim/DYToEE_madgraph.root") ;
//    TFile f_DYToEE_amc("ntuples/reskim/DYToEE_amcatnlo.root") ;
    TFile f_ZToTT("ntuples/reskim/ZToTT_amc_fakeRate_noTrig.root") ;
    TFile f_ST("ntuples/reskim/ST_fakeRate_noTrig.root") ;
    TFile f_ST_anti("ntuples/reskim/ST_anti_fakeRate_noTrig.root") ;
    TFile f_WZ   ("ntuples/reskim/WZ_fakeRate_noTrig.root"   ) ;
    TFile f_ZZ   ("ntuples/reskim/ZZ_fakeRate_noTrig.root"   ) ;

    TFile f_GJ1   ("ntuples/reskim/GamJet_40_100_FR.root"   ) ;
    TFile f_GJ2   ("ntuples/reskim/GamJet_100_200_FR.root"   ) ;
    TFile f_GJ3   ("ntuples/reskim/GamJet_200_400_FR.root"   ) ;
    TFile f_GJ4   ("ntuples/reskim/GamJet_400_600_FR.root"   ) ;
    TFile f_GJ5   ("ntuples/reskim/GamJet_600_Inf_FR.root"   ) ;
    
    TTree* t_ttbar = (TTree*) f_ttbar.Get("tap") ;
    TTree* t_WW    = (TTree*) f_WW   .Get("tap") ;
    TTree* t_WJets = (TTree*) f_WJets.Get("tap") ;
    TTree* t_ZToEE = (TTree*) f_ZToEE.Get("tap") ;

//    TTree* t_ZToEE = (TTree*) f_ZToEE.Get("tap") ;
//    TTree* t_DYToEE_mad = (TTree*) f_DYToEE_mad.Get("tap") ;
//    TTree* t_DYToEE_amc = (TTree*) f_DYToEE_amc.Get("tap") ;
    TTree* t_ZToTT = (TTree*) f_ZToTT.Get("tap") ;
    TTree* t_ST = (TTree*) f_ST.Get("tap") ;
    TTree* t_ST_anti = (TTree*) f_ST_anti.Get("tap") ;
    TTree* t_WZ    = (TTree*) f_WZ   .Get("tap") ;
    TTree* t_ZZ    = (TTree*) f_ZZ   .Get("tap") ;

    TTree* t_GJ1    = (TTree*) f_GJ1   .Get("tap") ;
    TTree* t_GJ2    = (TTree*) f_GJ2   .Get("tap") ;
    TTree* t_GJ3    = (TTree*) f_GJ3   .Get("tap") ;
    TTree* t_GJ4    = (TTree*) f_GJ4   .Get("tap") ;
    TTree* t_GJ5    = (TTree*) f_GJ5   .Get("tap") ;
    
    fh.Init(t_ttbar) ; fh.Loop("ttbar_FR") ;
    fh.Init(t_WW   ) ; fh.Loop("WW_FR"   ) ;
    fh.Init(t_WJets) ; fh.Loop("WJets_FR") ;
    fh.Init(t_ZToEE) ; fh.Loop("ZToEE_powheg_FR") ;

//    fh.Init(t_ZToEE) ; fh.Loop("ZToEE_powheg_ptw") ;
//    fh.Init(t_DYToEE_mad) ; fh.Loop("DYToEE_mad") ;
//    fh.Init(t_DYToEE_amc) ; fh.Loop("DYToEE_amc") ;
    fh.Init(t_ZToTT) ;  fh.Loop("ZToTT_FR") ;
    fh.Init(t_ST) ;     fh.Loop("ST_FR") ;
    fh.Init(t_ST_anti); fh.Loop("ST_anti_FR") ;
    fh.Init(t_WZ   ) ;  fh.Loop("WZ_FR"   ) ;
    fh.Init(t_ZZ   ) ;  fh.Loop("ZZ_FR"   ) ;
    fh.Init(t_GJ1   ) ; fh.Loop("GJ40_100_FR"   ) ;
    fh.Init(t_GJ2   ) ; fh.Loop("GJ100_200_FR"   ) ;
    fh.Init(t_GJ3   ) ; fh.Loop("GJ200_400_FR"   ) ;
    fh.Init(t_GJ4   ) ; fh.Loop("GJ400_600_FR"   ) ;
    fh.Init(t_GJ5   ) ; fh.Loop("GJ600_Inf_FR"   ) ;

  }
}

